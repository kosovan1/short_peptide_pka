# Load espresso, sugar and other necessary libraries

import espressomd
import numpy as np
import os
import sys
import csv
import espressomd.reaction_ensemble
from espressomd import interactions


metacentrum=False

if metacentrum:

    sugar_dir='/storage/brno2/home/blancoapa/shared/sugar_library'
else:
    
    path=os.getcwd()
    print('path: ', path)
    sugar_dir=path+'/../../sugar_library'
    print('sugar_dir: ', sugar_dir)
    gen_fun_dir=path+'/..gen_fun_dir'
    print('sugar_dir: ', sugar_dir)

sys.path.insert(0, sugar_dir)

import sugar
import general_functions

# The trajectories of the simulations will be stored using espresso built-up functions in separed files in the folder 'frames'
from espressomd.io.writer import vtf
if not os.path.exists('./frames'):
    os.makedirs('./frames')

# Simulation parameters

pH_value = PPPPP

Samples_per_pH=int( VVVVV)
MD_steps_per_sample=1000
steps_eq=int(Samples_per_pH/3)
N_samples_print= 100  # Write the trajectory every 100 samples
probability_reaction=0.5 

# Create an instance of sugar library
sg=sugar.sugar_library()

# Peptide parameters

sequence="SSSSS"
model='2beadAA'  # Model with 2 beads per each aminoacid
pep_concentration=1.56e-4 *sg.units.mol/sg.units.L
N_peptide_chains=1
residue_indices_to_track=RRRRR # Residue positions to calculate its average charge

    # Load peptide parametrization from Lunkad, R. et al.  Molecular Systems Design & Engineering (2021), 6(2), 122-131.

sg.load_parameters(filename=sugar_dir+'/reference_parameters/Lunkad2021.txt') 
sg.load_parameters(filename=sugar_dir+'/reference_parameters/Nozaki1967.txt')

    # Create an instance of a sugar molecule object for the peptide

peptide = sg.peptide(name='generic_peptide', sequence=sequence, model=model)

# Salt parameters
c_salt=5e-3 * sg.units.mol/ sg.units.L
cation=sg.particle(name='Na', type=sg.propose_unused_type(), q=1, diameter=0.35*sg.units.nm, epsilon=1*sg.units('reduced_energy'))
anion=sg.particle(name='Cl', type=sg.propose_unused_type(), q=-1, diameter=0.35*sg.units.nm,  epsilon=1*sg.units('reduced_energy'))

# System parameters
volume=N_peptide_chains/(sg.N_A*pep_concentration)
L=volume ** (1./3.) # Side of the simulation box
calculated_peptide_concentration=N_peptide_chains/(volume*sg.N_A)
dict_titrable_groups=sg.count_titrable_particles(object=peptide)
total_ionisible_groups=sum(dict_titrable_groups.values())
print("The box length of your system is", L.to('reduced_length'), L.to('nm'))
print('The peptide concentration in your system is ', calculated_peptide_concentration.to('mol/L') , 'with', N_peptide_chains, 'peptides')
print('The ionisable groups in your peptide is ', dict_titrable_groups)

    # Create an instance of an espresso system

system=espressomd.System(box_l=[L.to('reduced_length').magnitude]*3)

# Add all bonds to espresso system

sg.add_bonds_to_system(system=system)

# Create your molecules into the espresso system

for _ in range(N_peptide_chains):
    sg.create_object_in_system(object=peptide, system=system, use_default_bond=True)

sg.create_counterions_in_system(object=peptide,cation=cation,anion=anion,system=system) # Create counterions for the peptide chains
c_salt_calculated=sg.create_added_salt_in_system(system=system,cation=cation,anion=anion,c_salt=c_salt)

# Setup the acid-base reactions of the peptide using the constant pH ensemble

RE=sg.setup_constantpH_reactions(counter_ion=cation)

# Setup espresso to track the ionization of the acid/basic groups in peptide

sg.track_ionization(system=system)

# Setup the non-interacting type for speeding up the sampling of the reactions

type_dict=sg.get_all_stored_types()
non_interacting_type=max(type_dict.keys())+1
RE.set_non_interacting_type(non_interacting_type)
print('The non interacting type is set to ', non_interacting_type)

# Setup the potential energy

sg.setup_lj_interactions(system=system)
sg.setup_electrostatic_interactions(system=system, c_salt=c_salt)

# Minimize the system energy to avoid huge starting force due to random inicialization of the system

sg.minimize_system_energy(system=system)

# Setup espresso to do langevin dynamics

sg.setup_langevin_dynamics(system=system)

# Write the initial state

with open('frames/trajectory1.vtf', mode='w+t') as coordinates:
    vtf.writevsf(system, coordinates)
    vtf.writevcf(system, coordinates)


# Main loop for performing simulations at different pH-values

# output file with results of simulation
system_name = general_functions.get_system_name_peptide(seq = sequence,
                                model = model,
                                pH = pH_value, 
                                c_pep = pep_concentration,
                                c_salt = c_salt,
                                n_chains = N_peptide_chains,
                                )

if not os.path.exists('./'+system_name):
    #print ("make dir", system_name)
    os.makedirs(system_name)

output_filename = system_name+"/"+system_name+".csv" 
#print("output_filename: ", output_filename)
frames_dir = system_name+"/frames"

if not os.path.exists(frames_dir):
    #print ("make dir", frames_dir)
    os.makedirs(frames_dir)

# Write the initial state
with open(frames_dir+'/trajectory0.vtf', mode='w+t') as coordinates:
    vtf.writevsf(system, coordinates)
    vtf.writevcf(system, coordinates)

# define the observable names
system_observable_name_list = ["time","E_tot","E_kin","E_coul","E_nonbond","E_bond","T_kin"]
# use the function for calculating the charges to get observable names for individual side-chains
charges_dict = general_functions.calc_charges_per_side_chain(
        sg = sg, 
        system = system,
        molecule = peptide,
        residue_indices_to_track = residue_indices_to_track
        )
observable_name_list = system_observable_name_list + list( charges_dict.keys() )
print("observable_name_list:", observable_name_list)

f = open(output_filename, "w")
writer = csv.writer(f)
writer.writerow( observable_name_list )

# Sample list initialization
RE.constant_pH = pH_value

# Inner loop for sampling each pH value
frame=0
for step in range(Samples_per_pH+steps_eq):
    
    if np.random.random() > probability_reaction:
        system.integrator.run(steps=MD_steps_per_sample)
    else:
        RE.reaction(total_ionisible_groups)

    if ( step > steps_eq):
        time = system.time

        # Get peptide net charge

        Z_net = sg.get_net_charge(system=system, object=peptide)
        #Z_sim.append(np.mean(np.array(Z_net_list)))
        
        # Get the charge of the residues in residue_position
        Z_charge_res_dict = sg.get_charge_in_residues(system=system, molecule=peptide)
#        Z_groups_av=[]
#
#        z_pos=[]
#        for residue_position in residue_positions:
#            for molecule_dict in Z_charge_res_dict:
#                z_pos.append(molecule_dict[residue_position][sequence[residue_position]])
#        #print("z_pos: ", z_pos)
#            #Z_groups_av.append(np.mean(np.array(z_pos_av)))
#        #Z_groups_time_series.append(Z_groups_av)

        if (step % N_samples_print == 0):
            frame+=1
            with open('frames/trajectory'+str(frame)+'.vtf', mode='w+t') as coordinates:
                vtf.writevsf(system, coordinates)
                vtf.writevcf(system, coordinates)

        # calculate and write the observables
        observable_dict = general_functions.calc_system_obs(system, system_observable_name_list)
        charges_dict = general_functions.calc_charges_per_side_chain(
                sg = sg, 
                system = system,
                molecule = peptide,
                residue_indices_to_track = residue_indices_to_track
                )
        observable_dict.update( charges_dict )
        observable_values_list = general_functions.get_obs_values_list(obs_dict = observable_dict, obs_name_list = observable_name_list)
        writer.writerow( observable_values_list )

    sg.write_progress( step = step, total_steps = Samples_per_pH+steps_eq )

print("pH = {:6.4g} done".format(pH_value))

print("finished")
exit()
