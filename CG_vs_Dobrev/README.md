# CG vs Dobrev

* list of scripts and their purpose   
    
    * simulation scripts and related

        * META_peptide_simulation.py
            * constant-pH simulation (used in poster) 
            
    	* JOB-4.1.4.py
            * simulation in the metacentrum (used in poster) 

        * gen_pe.sh
            * simulation in the metacentrum (used in poster) 
    	

    * analyzing scripts

        * analyze-outputs.py
            * calculation of mean values with errorbars, output file - "analyzed_observables.csv" (used in poster)
    
    * plotting scripts  

        * plot_dobrev.py
            * plot values of degree of ionization with errorbars from Dobrev publication (used in bachelor thesis)

        * plot_ideal.py
            * plot ideal charge on the peptide and ideal degree of ionization (used in bachelor thesis)

        * plot_degree_ionization.py
            * plot ideal degree of ionization of ionizable group in peptide, degree of ionization of ionizable group in peptide (values from simulation) and dobrev values
            
    * scripts with functions

        * general_functions.py 
            * general functions used by various scripts, independent of Espresso

        * my_espresso_functions.py - not here but maybe it should be
            * functions used by various Espresso scripts

* other files and folders

    * analyzed_observables_ ... (4x - for each sequence)

        * analyzed data from our simulation (our CG)

    * ... _Dobrev_manuscript.csv (4x - for each sequence)

        * data from Dobrev manuscript, obtained from picture of plot using the website https://apps.automeris.io/wpd/

    * file poster.pdf 

        * poster presented in Berlin (September 2022)

    * poster_plots

        * plots used in poster
    
    * folder poster_materials

        * schemes of peptides in svg and pictures of peptides in png

    * zipped_trajectories

        * zipped vtf files

* obsolete and useless (I guess, not sure)

    * folder frames - with one file trajectory1.vtf (I think it is created during the simulation)
    
    * titration.py

