#!/usr/bin/env python3
# coding: utf-8

import numpy as np
import pandas as pd
from pathlib import Path
import os
import matplotlib.pyplot as plt

def plot_time_trace(filename, columns="all", mean_values = None, save_pdf = False):
    full_data = pd.read_csv(filename) # read the full trajectory as pandas dataframe 
    if(columns == "all"):
        columns = full_data.columns.to_list()
    for x in ['time']:
        if(x in columns):
            columns.remove(x)
    if(save_pdf):
            directory = "outputs_pdf"
            if(os.path.isdir(directory)==False):
                os.mkdir(directory)
    n_cols = len(columns)
    print("full_data:", full_data) 
    for col in columns:
        fig = plt.figure(figsize = (20,7))
        plt.plot(
            full_data['time'],
            full_data[col],
            linewidth = 1,
            marker = 'o',
            markersize = 3,
            alpha = 0.8,         
            color = 'blue',
            label = col
        )
        plt.xlabel('time [LJ tau]')
        plt.ylabel(col)
        plt.title(filename)
        if(save_pdf):           
            pdf_name = filename.name.replace(".csv", "_"+col+".pdf")
            print("pdf_name:", pdf_name)
            plt.savefig(os.path.join(directory,pdf_name))
            print("saved")
        plt.close()

def get_params_from_file_name(file_name):
    system_name = file_name.parts[-1].replace('.csv', '')
    print("system_name:", system_name)
    entries = system_name.split('_')
    #print(entries)
    params = {}
    for entry in entries:
        sp_entry = entry.split('-')
        if(sp_entry[0]=='N'):
            params[sp_entry[0]] = int(sp_entry[-1])
        else:
            params[sp_entry[0]] = sp_entry[-1]
    return params

def get_dt(data):
#    print("data:", data)
    print("data:", data)
    time = data['time']
    imax = data.shape[0]
    dt_init = time[1] - time[0]
    warn_lines = [];
    for i in range(1,imax):
        dt = time[i] - time[i-1]
        #print("i, t, dt:", i, time[i], dt)
        if(np.abs((dt_init - dt)/dt) > 0.01 ):
            warn_lines.append("Row {} dt = {} = {} - {} not equal to dt_init = {}\n".format(i, dt, time[i], time[i-1], dt_init)
            )
    if(len(warn_lines) > 20):
        print("\n*** Warning: Data file may be broken!*** \n\nFound {} data rows with inconsistent Delta t:\n".format(len(warn_lines))
             )
        for line in warn_lines:
            print(line)
    return dt

def new_index(data_frame, suffix, skip = ['time','step']):
    new_index = []
    for index in data_frame.index:
        if(index in skip):
            new_index.append(index)
        else:
            new_index.append(index+suffix)
#    print("new_index:", new_index)
    return(new_index)


def block_analyze(filename, n_blocks=16, equil=0.1):
    params = get_params_from_file_name(filename)    
    print("params:", params)
    full_data = pd.read_csv(filename) # read the full trajectory as pandas dataframe 
    #print("full_data:", full_data)
    dt = get_dt(full_data) # check that the data was stored with the same time interval dt
    drop_rows = int(full_data.shape[0]*equil) # calculate how many rows should be dropped as equilibration
    # drop the column time and rows that will be discarded as equlibration
    data = full_data.drop(columns=['time']).drop(range(0,drop_rows))
    # first, do the required operations on the remaining data
    n_samples = data.shape[0] # number of samples to be analyzed
    block_size = n_samples/n_blocks # mean block size
    mean = data.mean() # calculate the mean values of each column
    var_all = data.var() # calculate the variance of each column
    params['Blocks'] = n_blocks
    params['B_size'] = block_size
    print("b:", n_blocks, "k:", block_size)
    
    # calculate the mean per each block    
    blocks = np.array_split(data,n_blocks) # split the data array into blocks of (almost) equal size
    block_means = [] # empty list that we will use to store the means per each block
    for block in blocks:
        block_mean = block.mean() # mean values of each column within a given block
        block_means.append(block_mean)            
    block_means = pd.concat(block_means, axis=1).transpose()
    
    # perform calculations using averages or individual data blocks
    var_mean = block_means.var() # variance of the block averages = variance of the mean
    err_mean = np.sqrt(var_mean) # standard error of the mean
    tau_int = var_mean/var_all*block_size/2. # autocorrelation time in the unit of number of rows
    n_eff = n_samples/(2*tau_int) # effective number of samples
    tau_int = dt*tau_int # tau_int in the units of LJ time
    
    # modify the column names of the temporary results
    err_mean.index=new_index(err_mean,"Err")
    n_eff.index=new_index(n_eff,"Nef")
    tau_int.index=new_index(tau_int,"Tau")
    # first, concatenate the observables and alphabetically sort them by column names
    result = pd.concat( [ mean, err_mean, n_eff, tau_int ] ).sort_index(axis=0)
    # next, concatenate the results with simulation parameters to produce a human-readable output
    result = pd.concat( [ pd.Series(params), result] )
    return result


########################
# Key parameters
#n_blocks = [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8200] # uncomment this line if you want to see how n_blocks affects the result
n_blocks = [16] # default number of blocks in the block analysis
equil = 0.1 # fraction of of all data that should be discarded as equilibration

#filenames = list(Path('./').glob('Seq-*_pH-*_ca-*_cs-*_chains-*_N-*/*csv'))
seq = 'nGHAHGc'
folder = 'CG_vs_Dobrev/'+seq+'_data/'+seq+'_Dobrev'
filenames = list(Path(folder+'/').glob('Seq-'+seq+'-pH-*-run-1/Seq-'+seq+'_mod-*/*csv'))
plot = True # select if the plots should be displayed or not
output_file = "analyzed_observables_"+seq+"_Dobrev.csv"
########################

results = [] # empty list that we will use to store the results
for filename in filenames:
    params = get_params_from_file_name(filename)
    print("filename: ", filename)
    print("params: ", params)
    if(plot):
        plot_time_trace(filename)       
        # like this you can select specific columns to plot, and save the figure as pdf
        #plot_time_trace(filename, columns=['Rg_nm'], save_pdf = True) 
    print("start block_analysis")
    for n_b in n_blocks:
        tmp_result = block_analyze(
            filename = filename,
            n_blocks = n_b, 
            equil = equil, 
        )
        results.append(tmp_result)

results = pd.concat(results, axis=1).transpose()

print("final_results:\n", results)
results.to_csv(open(output_file,"w")) # save the results csv file that can be imported to spreadsheet calculators

f = open(output_file,'r')
temp = f.read()
f.close()
f = open(output_file,'w')
f.write("#" + temp)
f.close()

print("###\nFinished\n###")

