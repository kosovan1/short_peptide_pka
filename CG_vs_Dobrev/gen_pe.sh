#!/bin/bash

# `./gen_pe.sh` creates a separate directory for each run and prepares the scripts
# `./gen_pe.sh run` additionally submits the generated scripts into the queue

###espresso_version="4.1.2xx" # note: Espresso module 4.1.2 is available but it fails to load the analysis module
espresso_version="4.1.4"
#espresso_version="current-dev"

# resources for qsub
hour="0"
minute="30"
wt=$hour:$minute:00 # HH:MM:SS walltime
nodes=1
ncpus=1
queue="default"
mem="400mb"
scratch="400mb"

# parameters to sweep
pHs="4.1"
runs="1"

# testing parameters
pHs="3.33"
runs="1"

seqs="nGHAEGc"
residues="0, 2, 4, 6"
samples_per_pH="10"

# create templates and run
username=`whoami`
wdir="/storage/praha1/home/${username}/short_peptide_pka/CG_vs_Dobrev"
#wdir="/home/${username}/projects/short_peptide_pka/CG_vs_Dobrev"  #working directory on Magda's laptop
espresso_input="META_peptide_simulation.py"
sub_input="JOB-"${espresso_version}".sub"
flag=$1

for pH in $pHs; do
	for run in $(seq $runs); do
		# create a separate directory for each run
		dir_name="Seq-${seqs}-pH-${pH}-run-${run}"
		mkdir -p $dir_name
		cd $dir_name
		pwd

		# copy the espresso input and replace strings with values
		cp ${wdir}/${espresso_input} .
		sed -i "s/PPPPP/${pH}/g"  $espresso_input
		sed -i "s/SSSSS/${seqs}/g"  $espresso_input
		sed -i "s/RRRRR/${residues}/g"  $espresso_input
		sed -i "s/VVVVV/${samples_per_pH}/g"  $espresso_input
				
		# copy the espresso submission script and replace strings with values
		cp ${wdir}/${sub_input} .
		sed -i 's|EEEEE|'${espresso_input}'|g' $sub_input
		sed -i 's|DDDDD|'${wdir}'|g'           $sub_input
		sed -i 's|AAAAA|'${dir_name}'|g'       $sub_input

		# submit the job
		if [ "$flag" == "submit" ]; then
#			qsub -N $dir_name -l walltime=$wt -l select=$nodes:ncpus=$ncpus:mem=$mem:scratch_local=$scratch $sub_input
			qsub -N $dir_name -l walltime=$wt -l select=$nodes:ncpus=$ncpus:mem=$mem:scratch_local=$scratch:os=debian10:cl_adan=False:cl_hildor=False:cl_charon=False $sub_input
		fi
		cd $wdir
	done
done

exit 0
