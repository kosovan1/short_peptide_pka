import numpy as np

def get_system_name_cph(seq, pH, c_acid, c_salt, n_chains, chain_length ):
    return "Seq-{:s}_pH-{:.2f}_ca-{:.4f}_cs-{:.4f}_chains-{:.0f}_N-{:.0f}".format(seq, 
            pH, 
            c_acid.to("mol/L").magnitude, 
            c_salt.to('mol/L').magnitude,
            n_chains,
            chain_length
            )

def get_system_name_peptide(seq, model, pH, c_pep, c_salt, n_chains):
    return "Seq-{:s}_mod-{:s}_pH-{:.2f}_cp-{:.4f}_cs-{:.4f}_chains-{:.0f}".format(seq,
            model,
            pH, 
            c_pep.to("mol/L").magnitude, 
            c_salt.to('mol/L').magnitude,
            n_chains,
            )

def get_params_from_file_name(filename):
    return_dict = {}
    items = filename.rstrip('.csv').split('_')
    for item in items:
        values = item.split('-')
        return_dict[ values[0] ] = values[1]
    return return_dict

# the reference data from Henderson-Hasselbalch equation
def ideal_alpha(pH, pKa):
    return 1. / (1 + 10**(pKa - pH))

# statistical analysis of the results
def block_analyze(input_data, n_blocks=16):
    data = np.array(input_data)
    block = 0
    # this number of blocks is recommended by Janke as a reasonable compromise
    # between the conflicting requirements on block size and number of blocks
    block_size = int(data.shape[0] / n_blocks)
    print("block_size:", block_size)
    # initialize the array of per-block averages
    block_average = np.zeros((n_blocks, data.shape[0]))
    # calculate averages per each block
    for block in range(0, n_blocks):
        block_average[block] = np.average(data[block * block_size: (block + 1) * block_size])
    # calculate the average and average of the square
    av_data = np.average(data)
    av2_data = np.average(data * data)
    # calculate the variance of the block averages
    block_var = np.var(block_average)
    # calculate standard error of the mean
    err_data = np.sqrt(block_var / (n_blocks - 1))
    # estimate autocorrelation time using the formula given by Janke
    # this assumes that the errors have been correctly estimated
    tau_data = np.zeros(av_data.shape)
    if av_data == 0:
        # unphysical value marks a failure to compute tau
        tau_data = -1.0
    else:
        tau_data = 0.5 * block_size * n_blocks / (n_blocks - 1) * block_var / (av2_data - av_data * av_data)
    return av_data, err_data, tau_data, block_size

def check_electroneutrality(system,raise_error=True):
    Q=0;
    for p in system.part:
        Q+=p.q;
    print ("Check Q:",Q); 
    if(Q!=0 and raise_error):
        raise ValueError("System not neutral, Q="+str(Q)+"\n")
    return Q;

def get_obs_values_list(obs_dict, obs_name_list):
        obs_value_list = []
        for obs_name in obs_name_list:
            if obs_name in obs_dict:
                obs_value_list.append( obs_dict[ obs_name ] )
            else:
                obs_value_list.append( np.nan )
        return obs_value_list

def calc_system_obs(system, observables):
    ''' Calculate the values of observables, return a dictionary of their values '''
    energy=system.analysis.energy();
    all_res=[];
    obs_dict = {}
    for obs in observables:
        if ( obs == "time" ):
            obs_value = system.time;
        elif ( obs == "E_tot" ):
            obs_value = energy["total"];
        elif ( obs == "E_kin" ):
            obs_value = energy["kinetic"];
        elif ( obs == "E_bond" ):
            obs_value = energy["bonded"];
        elif ( obs == "E_nonbond" ):
            obs_value = energy["non_bonded"];
        elif ( obs == "E_coul" ):
            obs_value = energy["coulomb"];
        elif ( obs == "T_kin" ):
            obs_value = 2./3.* energy['kinetic']/len(system.part) 
        else:
            obs_value = np.nan;
        obs_dict[ obs ] = obs_value
    return obs_dict

def calc_charges_per_side_chain(sg, system, molecule, residue_indices_to_track):
   charges_dict = {}
   
   # get the net charge of the molecule
   charges_dict['q_net'] = sg.get_net_charge_from_espresso(espresso_system = system, sugar_object = molecule )[0]

   # Get the charge of the residues in residue_index
   charges_in_residues_per_molecule = sg.get_charge_in_residues(espresso_system = system, molecule = molecule)

   for charges_per_molecule in charges_in_residues_per_molecule:
       for residue_index in residue_indices_to_track:
           charges_per_residue = charges_per_molecule[residue_index]
           for side_chain in charges_per_residue:
               if side_chain != 'CA':
                   charges_dict[ "q_" + side_chain + str(residue_index) ] = np.array( charges_per_residue[ side_chain ] ).sum()
   return charges_dict
