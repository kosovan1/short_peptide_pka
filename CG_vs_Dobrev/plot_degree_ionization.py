import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import Path
import statistics 
from math import sqrt

import csv
import os
import sys

import general_functions
import plot_dobrev as pltd
import plot_ideal as pltid

def pGamma(I):
    ''' Extended DH formula for log of activity coefficient  '''
    A=0.5085 # mol^{1/2} dm^{3/2}
    B=0.3281 # nm^{-1} mol^{-1/2} dm^{3/2}
    a=0.9 # nm
    return A*sqrt(I)/(1+B*a*sqrt(I))
                

def plot_degree_ionization(I, data, x_variable = 'pH', y_variables = 'all', method_Dobrev = 'all', xlims = [0,14]): 

    ax,axis = plt.subplots(figsize=(6,4))
    
    skipped_columns = list(['#' , 'Seq', 'pH', 'ca', 'cs', 'chains', 'N', 'Blocks', 'B_size', 'mod', 'cp'])

    for Seq in csv_data["Seq"].unique():
        filtered_data_1 = csv_data[ csv_data['Seq'] == Seq ]

        legend_list = []

        pltid.plot_ideal_degree_ionization_aa(seq= Seq, legend_list=legend_list)
    
        for y_variable in y_variables:

            if (y_variable in skipped_columns):
                continue
            if (y_variable[-3:] == 'Err' or y_variable[-3:] == 'Nef' or y_variable[-3:] == 'Tau'):
                continue
            print("column: ", y_variable)
            
            for mod in csv_data["mod"].unique():
                filtered_data_2 = filtered_data_1[ filtered_data_1['mod'] == mod ]
                
                if (mod == '1beadAA'):
                    mode = 'one-bead model'
                elif (mod == '2beadAA'):
                    mode = 'CG'

                if (y_variable=='q_H2'):
                    first_legend = 'H2'
                    pKa = 6.54
                    colors = ["orange"]
                    legend_list.append(first_legend +' '+ mode)

                if (y_variable=='q_E2'):
                    first_legend = 'E2'
                    pKa = 4.08
                    colors = ["orange"]
                    legend_list.append(first_legend +' '+ mode)
                    filtered_data_2['q_E2'] = filtered_data_2['q_E2'].abs()

                if (y_variable=='q_H4'):
                    second_legend = 'H4'
                    pKa = 6.54
                    colors = ["green"]
                    legend_list.append(second_legend +' '+ mode)

                if (y_variable=='q_E4'):
                    second_legend = 'E4'
                    pKa = 4.08
                    colors = ["green"]
                    legend_list.append(second_legend +' '+ mode)
                    filtered_data_2['q_E4'] = filtered_data_2['q_E4'].abs()

                # save the original data for reference and for checking the correctness
                filtered_data_2['pH_orig'] = filtered_data_2['pH']
                # then overwrite the original data so that we get the corrected pH values 
                filtered_data_2['pH'] = filtered_data_2['pH'] + pGamma(I)
            
                ax = filtered_data_2.plot(
                                x=x_variable, 
                                y=y_variable,
                                yerr=y_variable+'Err',
                                xlabel='pH',
                                ylabel='Degree of ionization',
                                color=colors,
                                marker="o",
                                ms=6,
                                capsize=0,
                                linestyle="-",
                                legend = False,
                                alpha = 0.8,
                                ax=axis,               
                                )
                

            plt.rcParams.update({'font.size': 14})
            ax.set_xlabel('', fontdict={'fontsize':15})
            ax.set_ylabel('', fontdict={'fontsize':15})
            ax.tick_params(axis='both', labelsize=13)
            #ax.set_fontsize(24)
            plt.xlim(xlims)
            plt.title(Seq)
            plt.grid()
    
        pltd.plot_dobrev(seq=Seq, ax=ax, legend_list=legend_list, method = method_Dobrev, markers = markers, markersizes = markersizes)

        #box = ax.get_position()
        #ax.set_position([box.x0, box.y0, box.width * 0.95, box.height])
        axis.legend(legend_list, loc='center right',  bbox_to_anchor=(1.13,0.6), ncol=1)

        #plt.savefig(dir+'/'+Seq+'_mean_'+str(y_variable)+'_vs_'+x_variable+'.pdf')
        # FIXME all items from method_Dobrev should propagate to the file name
        plt.savefig(dir+'/'+Seq+'_degree_of_ionization_vs_'+x_variable+'_'+method_Dobrev+'.pdf', bbox_inches='tight')
        #plt.savefig(dir+'/'+Seq+'_degree_of_ionization_vs_'+x_variable+'_'+method_Dobrev+'.svg', bbox_inches='tight')
        #plt.savefig(dir+'/'+Seq+'_degree_of_ionization_vs_'+x_variable+'_'+method_Dobrev[0]+'.png', bbox_inches='tight')
        plt.cla()
         
#sg=sugar.sugar_library()

dir = os.path.join('plots_just_made') 
if not os.path.exists(dir):
    os.mkdir(dir)


seqs = ['nGEAEGc', 'nGHAHGc', 'nGHAEGc', 'nGEAHGc']
methods_Dobrev = ['NMR', 'MD']

xlims = {
        'nGEAEGc' : [1,7] , 
        'nGHAHGc' : [3,9] ,
        'nGHAEGc' : [1,9] , 
        'nGEAHGc' : [1,9] 
        }

markers = {
    'NMR': 'x',
    'MD' : 'o',
}

markersizes = {
    'NMR': 10,
    'MD' : 6,
}

I = 0.15 # mol/L

for seq in seqs:
    for method_Dobrev in methods_Dobrev:
        analyzed_obs_file_name = 'analyzed_observables_'+seq+'_Dobrev.csv'

        y_variables = [ 'q_'+seq[2]+'2', 'q_'+seq[4]+'4' ]
    
        # reading of data should be done outside the plotting function
        csv_data = pd.read_csv(analyzed_obs_file_name)
        
        #plot only two selected columns from the filtered data  
        plot_degree_ionization(I, csv_data , y_variables = y_variables, method_Dobrev = method_Dobrev, xlims = xlims[seq] )
        #plot_degree_ionization( csv_data , y_variables = ['Degree_ionization_first_group'] )
        #plot_degree_ionization( csv_data , y_variables = ['Degree_ionization_second_group'] )

print("finished")

