import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
import scipy.interpolate as spi

import sys
import os

import plot_ideal as pltid

def plot_dobrev(seq, ax, legend_list, method, markers, markersizes):
    csv_data = pd.read_csv(seq[1:6]+'_Dobrev_manuscript.csv')

    for Term in csv_data["Term"].unique():
        searchedTerm = Term
        if (Term != searchedTerm):
            continue

        filtered_data_1 = csv_data[ csv_data['Term'] == Term ]

        if (Term == 'C-term'):
            position = '4'
            color = ["green"]
        elif (Term == 'N-term'):
            position = '2'
            color = ["orange"]

        filtered_data_2 = filtered_data_1[ filtered_data_1['Method'] == method ]

        amAcid = filtered_data_2.iloc[0]['Aminoacid']
        if (amAcid == 'His'):
            amk = 'H'
        elif (amAcid == 'Glu'):
            amk = 'E'

        if (method == 'MD'):
            legend_method = 'AA'
        if (method == 'NMR'):
            legend_method = 'NMR'
        
        filtered_data_2.plot(
                        x='pH', 
                        y='Degree_ionization',
                        yerr=[filtered_data_2['Degree_ionizationErrDown'], filtered_data_2['Degree_ionizationErrUp']],
                        ylabel='Degree of ionization',
                        color=color,
                        marker=markers[method],
                        fillstyle='none',
                        markeredgewidth=1.5,
                        ms=markersizes[method],
                        capsize=0,
                        linestyle="",
                        alpha=0.7,
                        legend = False,
                        ax=ax,               
                        )
        
        plt.title(seq[1:6])
        plt.grid()

        legend_list.append(amk+position+' '+legend_method)

        #ax.legend(legend_list)
        ax.legend(legend_list, bbox_to_anchor=(0.7,0.8), ncol=1)
    

#dir = os.path.join('test_plots') 
#if not os.path.exists(dir):
#    os.mkdir(dir)
#
#seqs = ['nGEAEGc', 'nGHAHGc', 'nGHAEGc', 'nGEAHGc']
#methods_Dobrev = ['NMR', 'MD']
#
#xlims = {
#        'nGEAEGc' : [1,7] , 
#        'nGHAHGc' : [3,9] ,
#        'nGHAEGc' : [1,9] , 
#        'nGEAHGc' : [1,9] 
#        }
#
#markers = {
#    'NMR': 'x',
#    'MD' : 'o',
#}
#
#markersizes = {
#    'NMR': 10,
#    'MD' : 6,
#}
#
#ax,axis = plt.subplots(figsize=(6,4))
#
#for seq in seqs:
#    for method_Dobrev in methods_Dobrev:
#
#        legend_list = []
#
#        pltid.plot_ideal_degree_ionization_aa(seq= seq, legend_list=legend_list)
#        plot_dobrev(seq=seq, ax=axis, legend_list=[], method = method_Dobrev, markers = markers, markersizes = markersizes)
#
#        plt.savefig(dir+'/'+seq+'_Dobrev.pdf')
#        plt.cla()
