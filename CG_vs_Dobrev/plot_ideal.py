import matplotlib.pyplot as plt
import numpy as np
import sys
import os
import re
from math import sqrt


# FIXME this is not the right way of calling sugar library
sys.path.insert(0, './sugar')

#import sugar

#sg=sugar.sugar_library()
# FIXME the sugar library should be used

def plot_ideal_charge_of_peptide():
    for sequence in sequences:
        peptide = sg.molecule(sequence=sequence, model=model,  pKa_set=pKa_set)
        pH_range = np.linspace(1, 9, num=200)
        ideal_degree_ionization = sg.calculate_HH(sugar_object=peptide, pH=list(pH_range))

        ax = plt.plot(
                    pH_range,
                    ideal_degree_ionization,
                    marker="",
                    linestyle="-",
                    color="black"
                    )
        plt.xlabel('pH')
        plt.ylabel('Charge')
        plt.xlim(0,10)
        plt.title(sequence)
        plt.grid()
        #plt.savefig(dir+'/'+sequence+'_charge_ideal.pdf')
        #plt.cla()

def plot_ideal_degree_ionization_aa(seq, legend_list):
    pH_range = np.linspace(1, 9, num=200)

    ideal_degree_ionization_acid = 1/((10**(pka_acid-pH_range))+1) # equation of alpha (degree of ionization) for acid
    ideal_degree_ionization_base = 1/((10**(-pka_base+pH_range))+1) # equation of alpha (degree of ionization) for base

    searchSettings = [['.*E.*', '.*E.[EH].*', '.*[EH].E.*'], ['.*H.*', '.*H.[EH].*', '.*[EH].H.*']]
    searchMode = 0 # three modes available - "0", "1" and "2"
        # mode "0" - plot ideal degree of ionization for both position (2 and 4); in the case of sequence GHAEG, it plots histidin on position 2 and glutamic acid on position 4
        # mode "1" - plot ideal degree of ionization for position 2; in the case of sequence GHAEG, it plots only histidin on position 2 
        # mode "2" - plot ideal degree of ionization for position 4; in the case of sequence GHAEG, it plots only glutamic acid on position 4
    settingsList = []
    settingsList.append(searchSettings[0][searchMode])
    settingsList.append(searchSettings[1][searchMode])

    if (re.search(settingsList[0], seq)):
        plt.plot(
                    pH_range,
                    ideal_degree_ionization_acid,
                    marker="",
                    linestyle="--",
                    color="black",
                    alpha=0.8,
                    #ax=axis
                    )
        legend_list.append('Ideal Glu')
        
    if (re.search(settingsList[1], seq)):
        plt.plot(
                    pH_range,
                    ideal_degree_ionization_base,
                    marker="",
                    linestyle="-.",
                    color="black",
                    alpha=0.8,
                    #ax=ax
                    )
        legend_list.append('Ideal His')
    
    plt.xlabel('pH')
    plt.ylabel('Degree of ionization')
    plt.legend(legend_list)

    #plt.savefig(dir+'/'+seq+'.pdf')

#dir = os.path.join('grafy_mean_values_peptides') 
#if not os.path.exists(dir):
#    os.mkdir(dir)

#sequences = ['nGHAHGc','nGEAEGc', 'nGEAHGc', 'nGHAEGc']
sequences = ['GHAHG','GEAEG', 'GEAHG', 'GHAEG']
model = '2beadpeptide'
pKa_set='crc'

pka_base = 6.54
pka_acid = 4.08

ax,axis = plt.subplots(figsize=(6,4))

#plot_ideal_charge_of_peptide()
#plot_ideal_degree_ionization_aa(seq='GHAHG',legend_list=[])
#
#plt.title('Ideal Glu, Ideal His')
#plt.grid()
#plt.savefig(dir+'/'+'H_E'+'_charge_ideal.pdf')
