'''
*** written for ESPResSo 4.1.2
'''

import numpy as np
import sys
import math
import time
import os

import espressomd
from espressomd import analyze
from espressomd import integrate
from espressomd.interactions import *
from espressomd.io.writer import vtf
from espressomd import reaction_ensemble
from espressomd import electrostatics
from espressomd import features
from espressomd import checkpointing

### Process the input parameters
pH = PPPPP
seed = SSSSS

assert pH > 0 and seed > 0

### Hard-coded parameters

# map of particles
types = {
  'HA': 0, # acid uncharged
  'A' : 1, # acid charged
  'H' : 2, # H+ cation
}
charges = {
  'HA':  0.0,
  'A' : -1.0,
  'H' : +1.0,
}

# physics
N = 100 # acid species
box_l = 50 # box length
pK_A = 0 # pK_A of the acid
temperature = 1.0 # temperature

# electrostatics
l_bjerrum = 2.0 # bjerrum length
accuracy = 1e-3 # accuracy for the P3M

# non-bonden interactions
lj_eps = 1.0 # depth of LJ well in kT
lj_sig = 1.0 # LJ sigma
lj_cut = 1.12246 # LJ cutoff in σ, 2**(1./6) = 1.12246 for purely repulsive LJ (WCA)
lj_shift = 'auto' # offset for potential - 'auto' for continous potential on cutoff

# sampling
int_steps = 100 # integrator steps per production loop
warmup_steps = 100 # integrator steps per warm-up loop
rex_trials = 100 # reaction trials per production loop
SD_steps = 1000 # steepest descent integration steps per one call
main_loops = 100 # simulation loops

# output
obs_period = 10 # scalar observables
pos_period = 50 # trajectory frames
chk_period = 100 # checkpointing the system
rex_period = 10 # period of reaction steps

##### Checkpointing and I/O:
check_name = 'chk-res' # checkpoint directory
eng_file = open(os.path.abspath('.') + '/eng.dat', 'a') # energies 
pre_file = open(os.path.abspath('.') + '/pre.dat', 'a') # pressures
obs_file = open(os.path.abspath('.') + '/obs.dat', 'a') # custom observables
trj_file = open(os.path.abspath('.') + '/trj.vtf', 'a') # vtf trajectory

##### Preparation of the system:
if not os.path.exists(check_name):
  # if there isn't any checkpoint from a previous run, new system is created
  checkpoint = checkpointing.Checkpoint(checkpoint_id = check_name, checkpoint_path = '.')

  system = espressomd.System(box_l = [box_l,] * 3)
  system.time_step = 0.01
  system.cell_system.skin = 0.3 # empirical ... should be tuned

  #system.cell_system.max_num_cells = 2744 # empirical ... should be tuned (Pablo: Seems that this variable does not longer exist on the current develop version of espresso)
  st_step   = 0 # index of starting step for checkpoints
  sys_count = 0 # number of configuration moves
  rex_count = 0 # number of reaction moves

  # setup the system
  for i in range(N):
    system.part.add(
        pos = np.random.random(3) * system.box_l,
        id = len(system.part),
        type = types['A'],
        q = charges['A']
        )
    system.part.add(
        pos = np.random.random(3) * system.box_l,
        id = len(system.part),
        type = types['H'],
        q = charges['H']
        )
  
  # excluded volume interactions
  for t1 in types:
    for t2 in types:
      i = types[t1]
      j = types[t2]
      if(i <= j):
        system.non_bonded_inter[i, j].lennard_jones.set_params(epsilon = lj_eps, sigma = lj_sig, cutoff = lj_cut, shift = lj_shift)
  
  # minimalization with steepest descent to handle overlaps
  system.integrator.set_steepest_descent(f_max = 0.1, gamma = 0.1, max_displacement = 0.05)
  system.integrator.run(SD_steps)
  system.thermostat.set_langevin(kT = temperature, gamma = 1.0, seed = seed) # Pablo: the thermostat should be turned on after the steepest descent minimization
  
  # initial warm-up
  system.integrator.set_vv()
  system.integrator.run(warmup_steps)
  
  # electrostatics tuning to obtain parameters
  p3m = electrostatics.P3M(
          prefactor = l_bjerrum * temperature, 
          accuracy = accuracy
          )
  system.actors.add(p3m)

  # save the optimal parameters and add them by hand
  p3m_params = p3m.get_params()
  system.actors.remove(p3m)
  
  # electrostatics manual handling to deal with checkpointing
  p3m = electrostatics.P3M(
          prefactor = l_bjerrum * temperature, 
          accuracy = accuracy,
          mesh = p3m_params['mesh'],
          alpha = p3m_params['alpha'] ,
          cao = p3m_params['cao'],
          r_cut = p3m_params['r_cut'],
          tune = False
          )
  system.actors.add(p3m)

  # warm-up with electrostatics
  system.integrator.run(warmup_steps)
  
  # initial trajectory writeout
  vtf.writevsf(system, trj_file) # topology
  vtf.writevcf(system, trj_file) # positions
  trj_file.close()
  
  # checkpointing the system
  checkpoint.register("system")
  checkpoint.register("p3m_params")
  checkpoint.register("st_step")
  checkpoint.register("sys_count")
  checkpoint.register("rex_count")

  # creating structures for observables and checkpointing them
  obs_step = []
  checkpoint.register("obs_step")
  sys_time = []
  checkpoint.register("sys_time")
  RxMC_time = []
  checkpoint.register("RxMC_time")
  MD_time = []
  checkpoint.register("MD_time")
  energies_tot = []
  checkpoint.register("energies_tot")
  energies_kin = []
  checkpoint.register("energies_kin")
  energies_bon = []
  checkpoint.register("energies_bon")
  energies_nbd = []
  checkpoint.register("energies_nbd")
  energies_cou = []
  checkpoint.register("energies_cou")
  pressures_tot = []
  checkpoint.register("pressures_tot")
  pressures_kin = []
  checkpoint.register("pressures_kin")
  pressures_bon = []
  checkpoint.register("pressures_bon")
  pressures_nbd = []
  checkpoint.register("pressures_nbd")
  pressures_cou = []
  checkpoint.register("pressures_cou")
  alpha = []
  checkpoint.register("alpha")

  # final write-out
  checkpoint.save()

elif os.path.exists(check_name):
  # if checkpoint exists, system is loaded
  checkpoint = checkpointing.Checkpoint(checkpoint_id = check_name, checkpoint_path = '.')
  checkpoint.load()

### Constant-pH ensemble setup
RE = reaction_ensemble.ConstantpHEnsemble(temperature = temperature, exclusion_radius = 1, seed = seed)
RE.constant_pH = pH
RE.add_reaction(
	gamma = 10**(-pK_A),
	reactant_types = [ types['HA'] ],
	reactant_coefficients = [ 1 ],
	product_types = [ types['A'], types['H'] ],
	product_coefficients = [ 1, 1 ],
	default_charges={
		types['HA']: charges['HA'],
		types['A']:  charges['A'],
		types['H']:  charges['H']
	}
)

### Simulation

for step in range(st_step, main_loops + 1):
  # configuration move
  MD_t0 = time.perf_counter()
  system.integrator.run(int_steps)
  MD_t = time.perf_counter()
  MD_time.append(MD_t - MD_t0)
  sys_count += 1
  
  # reaction
  if(step % rex_period == 0):
    RxMC_t0 = time.perf_counter()
    RE.reaction(rex_trials)
    RxMC_t = time.perf_counter()
    RxMC_time.append(RxMC_t - RxMC_t0)
    rex_count += 1
  
  # analysis
  if(step % obs_period == 0):
    no_part = len(system.part)
    st_time = system.time
    energy = system.analysis.energy()
    pressure = system.analysis.pressure()
    
    HA = system.number_of_particles(type = types['HA'])
    A  = system.number_of_particles(type = types['A'])
    H  = system.number_of_particles(type = types['H'])
    fraction = A / (A + HA)

    obs_step.append(int(step / obs_period))
    sys_time.append(st_time)
    energies_tot.append(energy['total'])
    energies_kin.append(energy['kinetic'])
    energies_bon.append(energy['bonded'])
    energies_nbd.append(energy['non_bonded'])
    energies_cou.append(energy['coulomb'])
    pressures_tot.append(pressure['total'])
    pressures_kin.append(pressure['kinetic'])
    pressures_bon.append(pressure['bonded'])
    pressures_nbd.append(pressure['non_bonded'])
    pressures_cou.append(pressure['coulomb'])
    alpha.append(fraction)

  if(step % pos_period == 0):
    t_name = 'TRJ-' + '{:05d}'.format(int(step / pos_period)) + '.vtf'
    t_file = open(os.path.abspath('.') + '/' + t_name, 'a')
    vtf.writevsf(system, t_file)
    vtf.writevcf(system, t_file)
    t_file.close()

  if(step % chk_period == 0):
    st_step = step
    checkpoint.save()

# Dump the data
for i in range(len(obs_step)):
  obs_file.write(
          '{:d}\t{:5.1f}\t{:2.4f}\n'.format(
              obs_step[i],
              sys_time[i],
              alpha[i]
              )
          ) 
  pre_file.write(
          '{:d}\t{:5.1f}\t{:4.8f}\t{:4.8f}\t{:4.8f}\t{:4.8f}\t{:4.8f}\n'.format(
              obs_step[i],
              sys_time[i],
              pressures_tot[i],
              pressures_kin[i],
              pressures_bon[i],
              pressures_nbd[i],
              pressures_cou[i],
              )
          )
  eng_file.write(
          '{:d}\t{:5.1f}\t{:4.8f}\t{:4.8f}\t{:4.8f}\t{:4.8f}\t{:4.8f}\n'.format(
              obs_step[i],
              sys_time[i],
              energies_tot[i],
              energies_kin[i],
              energies_bon[i],
              energies_nbd[i],
              energies_cou[i],
              )
          )

print(' * RxMC trials: {:d}\n'.format(rex_trials * rex_count))
print(' * MD length in tau units: {:d}\n'.format(int(sys_count * int_steps * system.time_step) ))

RxMC_time = np.array((RxMC_time))
MD_time = np.array((MD_time))

print(' * time per RxMC step: {:4.4f} s\n'.format(RxMC_time.mean()))
print(' * time per MD step: {:4.4f} s\n'.format(MD_time.mean()))
print(' * cpH acceptance rate: {:2.4e}\n'.format(RE.get_acceptance_rate_reaction(0)))

eng_file.close()
pre_file.close()
obs_file.close()

exit()
