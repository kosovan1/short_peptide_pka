import matplotlib.pyplot as plt
import numpy as np
import sys
import os
import re

sys.path.insert(0, '../sugar_library')

import sugar

sg=sugar.sugar_library()

def ideal_alpha(pH, pKa):
    return 1. / (1 + 10**(pKa - pH))

def plot_ideal_charge(legend_list):
    #peptide = sg.peptide(name='generic_peptide', sequence=sequence, model=model)
    pH_range = np.linspace(1, 9, num=200)
    #ideal_charge = sg.calculate_HH(object=peptide, pH=list(pH_range))
    ideal_charge = -4.0*ideal_alpha(pH=pH_range, pKa=4.0)
    print("ideal_charge:", ideal_charge)

    plt.plot(
            pH_range,
            ideal_charge,
            marker="",
            linestyle="-",
            color="black",
            #ax=ax
            )
    plt.xlabel('pH')
    plt.ylabel('Charge on the peptide [e]')
    plt.xlim(0,10)
    plt.title(sequence)
    plt.grid()
    plt.savefig(dir+'/'+sequence+'_charge.pdf')
    #plt.cla()

    legend_list.append('ideal')

def plot_ideal_degree_of_ionization(legend_list):

    pH_range = np.linspace(1, 9, num=200)
    ideal_degree_ionization = ideal_alpha(pH=pH_range, pKa=4.0)

    plt.plot(
            pH_range,
            -ideal_degree_ionization,
            marker="",
            linestyle="-",
            color="black",
            #ax=ax
            )
    plt.xlabel('pH')
    plt.ylabel('Charge on the side chain [e]')
    plt.xlim(0,10)
    plt.title('Sequence '+sequence)
    plt.grid()
    plt.savefig(dir+'/'+sequence+'_degree_of_ionization.pdf')
    #plt.cla()

    legend_list.append('ideal')

dir = os.path.join('plots_DDDD') 
if not os.path.exists(dir):
    os.mkdir(dir)

sequence = ('DDDD')
model = '2beadAA'

#sg.load_parameters(filename='../sugar_library/reference_parameters/Hass2015.txt')

ax, axis = plt.subplots(figsize=(6,4))

legend_list=[]

#plot_ideal_degree_of_ionization(legend_list=legend_list)
plot_ideal_charge(legend_list=legend_list)

print ("finished")
