import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import Path
import statistics 

import csv
import os
import sys

#import plot_ideal as pltid

import pint  # module for working with units and dimensions
ureg = pint.UnitRegistry()
concentration = ureg.Quantity

def plot_all_columns_mean_values(data, x_variable = 'pH-pKa', y_variables = 'all'): 

    if ( y_variables == 'all' ):
        y_variables = csv_data.columns

    ax, axis = plt.subplots(figsize=(6,4))
    
    skipped_columns = list(['#' , 'Seq', 'pH', 'ca', 'cs', 'chains', 'N', 'Blocks', 'B_size', 'mod', 'cp'])

    for y_variable in y_variables:

        legend_list = []

        #pltid.plot_ideal_charge(legend_list=legend_list)

        if (y_variable in skipped_columns):
            continue
        if (y_variable[-3:] == 'Err' or y_variable[-3:] == 'Nef' or y_variable[-3:] == 'Tau'):
            continue
        print("column: ", y_variable)

        ax = csv_data.plot(
                    x=x_variable, 
                    y=y_variable,
                    yerr=y_variable+'Err',
                    ylabel='Charge on the peptide [e]',
                    marker="x",
                    linestyle="-",
                    legend = False,
                    alpha = 0.8,
                    ax=axis,               
                    )
            
        plt.xlim(-3.5,8.5)
        plt.title('Sequence DDDD')
        plt.grid()

        legend_list.append(y_variable)

        #box = ax.get_position()
        #ax.set_position([box.x0, box.y0, box.width * 0.95, box.height])
        axis.legend(legend_list, bbox_to_anchor=(0.9,0.6), ncol=1)

        plt.savefig(dir+'/DDDD_mean_'+str(y_variable)+'_vs_'+x_variable+'.pdf')
        
        plt.cla()
    #plt.savefig(dir+'/DDDD_all__vs_'+x_variable+'.pdf')

dir = os.path.join('plots_DDDD') 
if not os.path.exists(dir):
    os.mkdir(dir)

analyzed_obs_file_name = 'analyzed_observables_peptides.csv'

# reading of data should be done outside the plotting function
csv_data = pd.read_csv(analyzed_obs_file_name)

#plot only two selected columns from the filtered data 
plot_all_columns_mean_values( csv_data , y_variables = ['q_net'] )
#plot_all_columns_mean_values( csv_data , y_variables = ['q_D0','q_D1','q_D2','q_D3'] )

print("finished")

