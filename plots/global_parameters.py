# global parameters
plot_styles = {}
plot_styles["Raju"]  = {"color": "red",    "marker": "o", "fillstyle":'none', "linestyle":'none', 'markersize':10, 'zorder':10}
plot_styles["Pablo"] = {"color": "green",  "marker": "s", "fillstyle":'none', "linestyle":'none', 'markersize':10, 'zorder':10}
plot_styles["FPTS"]  = {"color": "blue",   "marker": "s", "fillstyle":'none', "linestyle":'none', 'markersize':6, 'zorder':8}
plot_styles["HH"]  = {"color": "black",   "marker": "", "fillstyle":'none', "linestyle":'-', 'markersize':4}
plot_styles["CG"]  = {"color": "red",    "marker": "o", "fillstyle":'none', "linestyle":'none', 'markersize':10, 'zorder':10}
plot_styles["Indv0"]  = {"color": "red",  "fillstyle":'none',  }
plot_styles["Indv1"]  = {"color": "blue", "fillstyle":'none',  }
plot_styles["Indv2"]  = {"color": "green", "fillstyle":'none', }
plot_styles["Indv3"]  = {"color": "orange", "fillstyle":'none',}
plot_styles["M: Ideal"]  = {"color": "blue",   "marker": "s", "fillstyle":'none', "linestyle":'none', 'markersize':6, 'zorder':8}
plot_styles["M: Real"] = {"color": "green",  "marker": "s", "fillstyle":'none', "linestyle":'none', 'markersize':10, 'zorder':10}
plot_styles["P: Real"]  = {"color": "red",    "marker": "o", "fillstyle":'none', "linestyle":'none', 'markersize':10, 'zorder':10}


plot_styles["IdealAcid"]  = {"color": "red",    "marker": "o", "fillstyle":'none', "linestyle":'--', 'markersize':7, 'zorder':10}
plot_styles["IdealBase"]  = {"color": "blue",   "marker": "s", "fillstyle":'none', "linestyle":'--', 'markersize':7, 'zorder':8}

