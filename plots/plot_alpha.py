import sys, csv
import numpy as np
import matplotlib.pyplot as plt
import os
import global_parameters as glob
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages

def calculate_HH(pH=None, pKa_value=None, gained_charge=None, MPC=None):
    data = {}
    data["pH"] = pH
    z_analyte = 0.
    #for N in MPC:
    alpha = {}
    conv = 1.0 / (1.0 + np.power(10, (pKa_value - pH)))
    if gained_charge < 0:
        data["alpha_acid"] = conv
        z_analyte += data["alpha_acid"] * gained_charge * MPC
    else:
        data["alpha_base"] = (1-conv)
        z_analyte += data["alpha_base"] * gained_charge * MPC
    data["q_net"] = z_analyte
    #dpH, dz = get_derv(x=data["pH"], y=data["q_net"], order=order)
    #data["C".format(order)] = np.array(dz)*(1./np.log(10))
    #data["cpH".format(order)] = dpH
    return data

def corrected_HH(pH=None, pKa_value=None, gained_charge=None, MPC=None, I=None):
    data = {}
    data["pH"] = pH
    z_analyte = 0.
    #for N in MPC:
    alpha = {}
    A = -0.51*(np.sqrt(I)/(1.+np.sqrt(I)) -0.3*I) # Activity coefficient of ionic solutions using the semi-empirical formula by Davies
    #a=3 # the size or ons bind water less tightly and have smaller hydrated radii than smaller, more highly charged ions. Typical values are 3Å for ions such as H+, Cl−    
##    A = -0.51*(np.sqrt(I)/(1.+ 0.3281*a*np.sqrt(I))) 
    
    #A = -0.51*(np.sqrt(I))    # D-H limiting law
    print(A, 10**A)
    if gained_charge < 0:
        conv = 1.0 / (1.0 + np.power(10, (pKa_value - pH + A)))
        data["alpha_acid"] = conv
        z_analyte += data["alpha_acid"] * gained_charge * MPC
    else:
        conv = 1.0 / (1.0 + np.power(10, (pKa_value - pH - A)))
        data["alpha_base"] = (1-conv)
        z_analyte += data["alpha_base"] * gained_charge * MPC
    data["q_net"] = z_analyte
    #dpH, dz = get_derv(x=data["pH"], y=data["q_net"], order=order)
    #data["C".format(order)] = np.array(dz)*(1./np.log(10))
    #data["cpH".format(order)] = dpH
    return data


def plot_charge(
        acid_data=None,
        base_data=None,        
        z_min=None,
        z_max=None,
        fontsize=9.0,
        cs_salt=np.nan,
        ):
    fig,ax1 = plt.subplots()
    # twin object for two different y-axis on the sample plot
    ax2=ax1.twinx()
    pKa = 0.0
    pKb = 0.0
    N_acid=0
    n_acid=0
    N_base=0
    n_base=0
    system_type_acid = 'NaN'
    system_type_base = 'NaN'    
    c_salt_acid = 0.0
    c_salt_base = 0.0    
    if acid_data is not None:
        acid_data = acid_data.sort_values(by='pH', ascending=True)
        pKa = np.unique(acid_data['pKa'])[0]
        c_salt_acid = np.unique(acid_data['cS'])[0]
        N_acid = np.unique(acid_data['N'])[0]
        n_acid = np.unique(acid_data['n'])[0]
        system_type_acid = np.unique(acid_data['cpH'])[0]        
        gained_charge_acid = -1.0
        HH = calculate_HH(pH=np.linspace(2, 12, 100), pKa_value=pKa, gained_charge=gained_charge_acid, MPC=N_acid)
        xdata = HH['pH'] - pKa
        # HH        
        ax1.plot(
            xdata,
            HH['q_net']/N_acid,
            label='HH',
            **glob.plot_styles['HH']
        )
        # HH corrected
        HH_corrected = corrected_HH(pH=np.linspace(2, 12, 100), pKa_value=pKa, gained_charge=gained_charge_acid, MPC=N_acid, I=c_salt_acid)
        ax1.plot(
            HH_corrected['pH']-pKa,
            HH_corrected['q_net']/N_acid,
            label='HH+ DH',
            linestyle=':',
            color='red'
         )
        
        # simulations
        xdata = acid_data['pH'] - pKa
        ydata = acid_data['alpha_acid']*gained_charge_acid
        yerr = acid_data['alpha_acidErr']*gained_charge_acid
        ax1.errorbar(
                xdata,
                ydata,
                yerr,
                label='Simulation',
                **glob.plot_styles['IdealAcid']
        )
    # base
    if base_data is not None:
        base_data = base_data.sort_values(by='pH', ascending=True)
        pKb = np.unique(base_data['pKb'])[0]            
        c_salt_base = np.unique(base_data['cS'])[0]
        N_base = np.unique(base_data['N'])[0]
        n_base = np.unique(base_data['n'])[0]
        system_type_base = np.unique(base_data['cpH'])[0]
        gained_charge_base = 1.0
        HH = calculate_HH(pH=np.linspace(2, 12, 100), pKa_value=pKb, gained_charge=gained_charge_base, MPC=N_base)
        xdata = HH['pH'] - pKb    
        ydata = HH['q_net']/N_base
        # HH        
        ax2.plot(
            xdata,
            ydata,
            label='HH',
            **glob.plot_styles['HH']
        )
        HH_corrected = corrected_HH(pH=np.linspace(2, 12, 100), pKa_value=pKb, gained_charge=gained_charge_base, MPC=N_acid, I=c_salt_base)
        ax2.plot(
            HH_corrected['pH']-pKb,
            HH_corrected['q_net']/N_acid,
            label='HH+ DH',
            linestyle=':',
            color='blue'            
         )
        
        system_type_base = np.unique(base_data['cpH'])[0]            
        xdata = base_data['pH'] - pKb
        ydata = base_data['alpha_base']*gained_charge_base
        yerr = base_data['alpha_baseErr']*gained_charge_base
        ax2.errorbar(
            xdata,
            ydata,
            yerr,
            label='Simulation',
            **glob.plot_styles['IdealBase']
        )            
    #    plt.title('{}, pK$_\mathrm{{A}}$: {}, pK$_\mathrm{{B}}$: {}, $c_{{\mathrm{{s}}}}$ [M]: {}, $N_A$: {}, $n_A$ : {}, $N_B$: {}, $n_B$: {}'.format(system_type, pKa,pKb, c_salt, N_acid, n_acid, N_base, n_base), fontsize=10)
    plt.title('Acid: {}, pK$_\mathrm{{A}}$: {},   $c_{{\mathrm{{s}}}}$ [M]: {}, $N_A$: {}, $n_A$ : {} \nBase: {}, pK$_\mathrm{{B}}$: {},   $c_{{\mathrm{{s}}}}$ [M]: {}, $N_B$: {}, $n_B$ : {}'.format(system_type_acid, pKa, c_salt_acid, N_acid, n_acid, system_type_base, pKb, c_salt_base, N_base, n_base), fontsize=10)        
    ax1.set_ylabel("Total charge on acid Sequence [e]", fontsize=fontsize, color='red')
    ax2.set_ylabel("Total charge on base Sequence [e]", fontsize=fontsize, color='blue')    
    ax1.tick_params(axis='y', colors='red')
    ax2.tick_params(axis='y', colors='blue')    
    ax1.grid()
    ax2.axhline(y=0.5,  color="gray", linestyle="--", lw=2.0)
    ax1.set_xlabel(r"$\mathrm{pH} - \mathrm{p}K$", fontsize=fontsize)
    ax1.axvline(x=0.0,  color="gray", linestyle="--", lw=2.0)
    ax1.set_xlim(-4, 4)
    ax1.set_ylim(-1.1, 0.1)
    ax2.set_ylim(-0.1, 1.1)

def plot_alpha(
        acid_data=None,
        base_data=None,
        z_min=None,
        z_max=None,
        fontsize=9.0,
        cs_salt=np.nan,
        ):
    fig,ax1 = plt.subplots()
    # twin object for two different y-axis on the sample plot
    pKa = 0.0
    pKb = 0.0
    N_acid=0
    n_acid=0
    N_base=0
    n_base=0
    system_type_acid = 'NaN'
    system_type_base = 'NaN'
    c_salt_acid = 0.0
    c_salt_base = 0.0
    if acid_data is not None:
        acid_data = acid_data.sort_values(by='pH', ascending=True)
        pKa = np.unique(acid_data['pKa'])[0]
        c_salt_acid = np.unique(acid_data['cS'])[0]
        N_acid = np.unique(acid_data['N'])[0]
        n_acid = np.unique(acid_data['n'])[0]
        system_type_acid = np.unique(acid_data['cpH'])[0]
        gained_charge_acid = -1.0
        HH = calculate_HH(pH=np.linspace(2, 12, 100), pKa_value=pKa, gained_charge=gained_charge_acid, MPC=N_acid)
        xdata = HH['pH'] - pKa
        # HH        
        ax1.plot(
            xdata,
            HH['alpha_acid'],
            label='HH: acid',
            **glob.plot_styles['HH']
        )
        # simulations
        xdata = acid_data['pH'] - pKa
        ydata = acid_data['alpha_acid']
        yerr = acid_data['alpha_acidErr']
        ax1.errorbar(
                xdata,
                ydata,
                yerr,
                label='Sim: acid ',
                **glob.plot_styles['IdealAcid']
        )
    # base
    if base_data is not None:
        base_data = base_data.sort_values(by='pH', ascending=True)
        pKb = np.unique(base_data['pKb'])[0]
        c_salt_base = np.unique(base_data['cS'])[0]
        N_base = np.unique(base_data['N'])[0]
        n_base = np.unique(base_data['n'])[0]
        system_type_base = np.unique(base_data['cpH'])[0]
        gained_charge_base = 1.0
        HH = calculate_HH(pH=np.linspace(2, 12, 100), pKa_value=pKb, gained_charge=gained_charge_base, MPC=N_base)
        xdata = pKb - HH['pH']# - pKb
        ydata = HH['alpha_base']
        # HH        
        ax1.plot(
            xdata,
            ydata,
            label='HH: base',
            **glob.plot_styles['HH']
        )
        c_salt_base = np.unique(base_data['cS'])[0]
        N_base = np.unique(base_data['N'])[0]
        n_base = np.unique(base_data['n'])[0]
        system_type_base = np.unique(base_data['cpH'])[0]
        xdata = pKb - base_data['pH']# - pKb
        ydata = base_data['alpha_base']
        yerr = base_data['alpha_baseErr']
        ax1.errorbar(
            xdata,
            ydata,
            yerr,
            label='Sim: base',
            **glob.plot_styles['IdealBase']
        )
    #    plt.title('{}, pK$_\mathrm{{A}}$: {}, pK$_\mathrm{{B}}$: {}, $c_{{\mathrm{{s}}}}$ [M]: {}, $N_A$: {}, $n_A$ : {}, $N_B$: {}, $n_B$: {}'.format(system_type, pKa,pKb, c_salt, N_acid, n_acid, N_base, n_base), fontsize=10)
    plt.title('Acid: {}, pK$_\mathrm{{A}}$: {},   $c_{{\mathrm{{s}}}}$ [M]: {}, $N_A$: {}, $n_A$ : {} \nBase: {}, pK$_\mathrm{{B}}$: {},   $c_{{\mathrm{{s}}}}$ [M]: {}, $N_B$: {}, $n_B$ : {}'.format(system_type_acid, pKa, c_salt_acid, N_acid, n_acid, system_type_base, pKb, c_salt_base, N_base, n_base), fontsize=10)
    ax1.set_ylabel("Degree of Ionization", fontsize=fontsize, color='black')
    #ax1.tick_params(axis='y', colors='red')
    ax1.grid()
    ax1.axhline(y=0.5,  color="gray", linestyle="--", lw=2.0)
    ax1.set_xlabel(r"$\mathrm{pH} - \mathrm{p}K$", fontsize=fontsize)
    ax1.axvline(x=0.0,  color="gray", linestyle="--", lw=2.0)
    ax1.set_xlim(-4, 4)
    ax1.set_ylim(-0.1, 1.1)


fontsize=12
cs_salts = [0.01, 0.15]
n_particles=20
Ns = [ 1] # MPC
if __name__ == "__main__":
    output_file, _ = os.path.splitext(os.path.basename(__file__))
    with PdfPages(output_file + ".pdf") as pdf:
        for cs_salt in cs_salts:        
#            plt.rc("font", family='sans', size=fontsize)
#            plt.rc("xtick", labelsize=fontsize)
#            plt.rc("ytick", labelsize=fontsize)
#            plt.rc("text", usetex=False)
#            plt.rc('text.latex')
#            plt.ticklabel_format(style='sci', axis='y', scilimits=(0.1,10))        
#            acid_file_Path = '../data/cpH-ideal_N-1_n-20_cM-0.0100_pKa-4.00_pKb-0.00_cS-{}.csv'.format(cs_salt)
#            acid_data=pd.read_csv(acid_file_Path)
#            base_file_Path = '../data/cpH-ideal_N-1_n-20_cM-0.0100_pKa-0.00_pKb-6.30_cS-{}.csv'.format(cs_salt)
#            base_data=pd.read_csv(base_file_Path)         
#            # Plot total charge
#            plot_charge(
#                    acid_data=acid_data,
#                    base_data=base_data,                        
#                    fontsize=fontsize,
#                    cs_salt=cs_salt,
#            )       
#            loc = 'upper right'
#            plt.legend(loc=loc,fontsize=fontsize,facecolor='White',framealpha=1.0,edgecolor='white',)
#            pdf.savefig()
#            plt.close()            
            for N in Ns:
                n=int(n_particles/N)
                acid_file_Path = '../data/cpH-real_N-{}_n-{}_cM-0.0100_pKa-4.00_pKb-0.00_cS-{}.csv'.format(N, n, cs_salt)                
                acid_data=pd.read_csv(acid_file_Path)
                base_file_Path = '../data/cpH-real_N-{}_n-{}_cM-0.0100_pKa-0.00_pKb-6.30_cS-{}.csv'.format(N, n, cs_salt)
                base_data=pd.read_csv(base_file_Path)                
                #################
                plot_charge(
                        acid_data=acid_data,
                        base_data=base_data,                        
                        fontsize=fontsize,
                        cs_salt=cs_salt,
                )
                loc = 'upper right'
                plt.legend(loc=loc,fontsize=fontsize,facecolor='White',framealpha=1.0,edgecolor='white',)                
                pdf.savefig()
                plt.close()                
#            for N in Ns:
#                n=int(n_particles/N)
#                acid_file_Path = '../data/cpH-real_N-{}_n-{}_cM-0.0100_pKa-4.00_pKb-0.00_cS-{}.csv'.format(N, n, cs_salt)
#                acid_data=pd.read_csv(acid_file_Path)
#                base_file_Path = '../data/cpH-real_N-{}_n-{}_cM-0.0100_pKa-0.00_pKb-6.30_cS-{}.csv'.format(N, n, cs_salt)
#                base_data=pd.read_csv(base_file_Path)
#                #################                
#                plot_alpha(
#                        acid_data=acid_data,
#                        base_data=base_data,
#                        fontsize=fontsize,
#                        cs_salt=cs_salt,
#                )
#                loc = 'upper left'
#                plt.legend(loc=loc,fontsize=fontsize,facecolor='White',framealpha=1.0,edgecolor='white',)
#                pdf.savefig()
#                plt.close()
#
