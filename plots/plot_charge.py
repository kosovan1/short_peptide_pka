import sys, csv
import numpy as np
import matplotlib.pyplot as plt
import os
import global_parameters as glob
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages

def calculate_HH(pH=None, pKa_value=None, gained_charge=None, MPC=None):
    data = {}
    data["pH"] = pH
    z_analyte = 0.
    #for N in MPC:
    alpha = {}
    if gained_charge < 0:
        conv = 1.0 / (1.0 + np.power(10, (pKa_value - pH)))
        data["alpha_acid"] = conv
        z_analyte += data["alpha_acid"] * gained_charge * MPC
    else:
        conv = 1.0 / (1.0 + np.power(10, (pKa_value - pH)))        
        data["alpha_base"] = (1-conv)
        z_analyte += data["alpha_base"] * gained_charge * MPC
    data["q_net"] = z_analyte
    #dpH, dz = get_derv(x=data["pH"], y=data["q_net"], order=order)
    #data["C".format(order)] = np.array(dz)*(1./np.log(10))
    #data["cpH".format(order)] = dpH
    return data

def corrected_HH(pH=None, pKa_value=None, gained_charge=None, MPC=None, I=None):
    data = {}
    data["pH"] = pH
    z_analyte = 0.
    #for N in MPC:
    alpha = {}
    A = -0.51*(np.sqrt(I)/(1.+np.sqrt(I)) -0.3*I)
    print(A)
    if gained_charge < 0:
        conv = 1.0 / (1.0 + np.power(10, (pKa_value - pH - A)))
        data["alpha_acid"] = conv
        z_analyte += data["alpha_acid"] * gained_charge * MPC
    else:
        conv = 1.0 / (1.0 + np.power(10, (pKa_value - pH + A)))
        data["alpha_base"] = (1-conv)
        z_analyte += data["alpha_base"] * gained_charge * MPC
    data["q_net"] = z_analyte
    #dpH, dz = get_derv(x=data["pH"], y=data["q_net"], order=order)
    #data["C".format(order)] = np.array(dz)*(1./np.log(10))
    #data["cpH".format(order)] = dpH
    return data


def plot_charge(
        acid_datas=None,
        base_datas=None,        
        z_min=None,
        z_max=None,
        fontsize=9.0,
        cs_salts=None,
        MPCs=None,
        ):
    fig,ax1 = plt.subplots()
    # twin object for two different y-axis on the sample plot
    #ax2=ax1.twinx()
    pKa = 0.0
    pKb = 0.0
    N_acid=0
    n_acid=0
    N_base=0
    n_base=0
    system_type_acid = 'NaN'
    system_type_base = 'NaN'    
    c_salt_acid = 0.0
    c_salt_base = 0.0    
    # acid
    if acid_datas is not None:
        for acid_data in acid_datas:
            acid_data = acid_data.sort_values(by='pH', ascending=True)
            pKa = np.unique(acid_data['pKa'])[0]
            c_salt_acid = np.unique(acid_data['cS'])[0]
            N_acid = np.unique(acid_data['N'])[0]
            n_acid = np.unique(acid_data['n'])[0]
            system_type_acid = np.unique(acid_data['cpH'])[0]
            gained_charge_acid = -1.0
            HH = calculate_HH(pH=np.linspace(2, 12, 100), pKa_value=pKa, gained_charge=gained_charge_acid, MPC=N_acid)
            HH_corrected = corrected_HH(pH=np.linspace(2, 12, 100), pKa_value=pKa, gained_charge=gained_charge_acid, MPC=N_acid, I=c_salt_acid)     
            ax1.plot(
                HH_corrected['pH']-pKa,
                HH_corrected['q_net']/N_acid,
                label='HH-corrected',
             )
            
            system_type_acid = np.unique(acid_data['cpH'])[0]
            xdata = acid_data['pH'] - pKa
            ydata = acid_data['alpha_acid']*gained_charge_acid
            yerr = acid_data['alpha_acidErr']*gained_charge_acid
            if cs_salts is not None:
                label='$c_{{\mathrm{{s}}}}$ [M]: {}'.format(c_salt_acid)
                plt.title('Acid: {}, pK$_\mathrm{{A}}$: {}, $N_A$: {}, $n_A$ : {}'.format(
                    system_type_acid, pKa, N_acid, n_acid), fontsize=10)
            if MPCs is not None:
                label='N: {}'.format(N_acid)
                plt.title('Acid: {}, pK$_\mathrm{{A}}$: {},   $c_{{\mathrm{{s}}}}$ [M]: {}'.format(
                    system_type_acid, pKa, c_salt_acid), fontsize=10)
            ax1.errorbar(
                xdata,
                ydata,
                yerr,
                label=label,
                linestyle='--',
                marker='o',
                markersize=8,
                fillstyle='none'
            )
        xdata = HH['pH'] - pKa
        ax1.plot(
            xdata,
            HH['q_net']/N_acid,
            label='HH',
            **glob.plot_styles['HH']
        )
        ax1.set_ylim(-1.1, 0.1)        
        ax1.axhline(y=-0.5,  color="gray", linestyle="--", lw=2.0)    
    # base
    if base_datas is not None:
        for base_data in base_datas:
            base_data = base_data.sort_values(by='pH', ascending=True)
            pKb = np.unique(base_data['pKb'])[0]            
            c_salt_base = np.unique(base_data['cS'])[0]
            N_base = np.unique(base_data['N'])[0]
            n_base = np.unique(base_data['n'])[0]
            system_type_base = np.unique(base_data['cpH'])[0]
            gained_charge_base = 1.0
            HH = calculate_HH(pH=np.linspace(2, 12, 100), pKa_value=pKb, gained_charge=gained_charge_base, MPC=N_base)
            c_salt_base = np.unique(base_data['cS'])[0]
            N_base = np.unique(base_data['N'])[0]
            n_base = np.unique(base_data['n'])[0] 
            system_type_base = np.unique(base_data['cpH'])[0]            
            xdata = base_data['pH'] - pKb
            ydata = base_data['alpha_base']*gained_charge_base
            yerr = base_data['alpha_baseErr']*gained_charge_base
            if cs_salts is not None:
                label='$c_{{\mathrm{{s}}}}$ [M]: {}'.format(c_salt_base)
                plt.title('Base: {}, pK$_\mathrm{{B}}$: {}, $N_B$: {}, $n_B$ : {}'.format(
                    system_type_base, pKb, N_base, n_base), fontsize=10)                
            if MPCs is not None:
                label='N: {}'.format(N_base)
                plt.title('Base: {}, pK$_\mathrm{{B}}$: {},   $c_{{\mathrm{{s}}}}$ [M]: {}'.format(
                    system_type_base, pKb, c_salt_base), fontsize=10)                
                
            ax1.errorbar(
                xdata,
                ydata,
                yerr,
                label=label,
                linestyle='--',
                marker='o',
                fillstyle='none'
            )           
        xdata = HH['pH'] - pKb
        # HH        
        ax1.plot(
            xdata,
            HH['q_net']/N_base,
            label='HH',
            **glob.plot_styles['HH']
        )
        ax1.set_ylim(-0.1, 1.1)        
        ax1.axhline(y=0.5,  color="gray", linestyle="--", lw=2.0)        
#        plt.title('Acid: {}, pK$_\mathrm{{A}}$: {},   $c_{{\mathrm{{s}}}}$ [M]: {}, $N_A$: {}, $n_A$ : {} \nBase: {}, pK$_\mathrm{{B}}$: {},   $c_{{\mathrm{{s}}}}$ [M]: {}, $N_B$: {}, $n_B$ : {}'.format(system_type_acid, pKa, c_salt_acid, N_acid, n_acid, system_type_base, pKb, c_salt_base, N_base, n_base), fontsize=10)        
    ax1.set_ylabel("charge on Sequence [e]", fontsize=fontsize, color='black')
    ax1.grid()
    ax1.set_xlabel(r"$\mathrm{pH} - \mathrm{p}K$", fontsize=fontsize)
    ax1.axvline(x=0.0,  color="gray", linestyle="--", lw=2.0)
    ax1.set_xlim(-4, 4)

fontsize=12
cs_salts = [0.01, 0.15]
n_particles=20
Ns = [1, 4, 10, 20] # MPC
if __name__ == "__main__":
    output_file, _ = os.path.splitext(os.path.basename(__file__))
    with PdfPages(output_file + ".pdf") as pdf:
        acid_datas=[]
        plt.rc("font", family='sans', size=fontsize)
        plt.rc("xtick", labelsize=fontsize)
        plt.rc("ytick", labelsize=fontsize)
        plt.rc("text", usetex=False)
        plt.rc('text.latex')
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0.1,10))        
        ##########################################
#        # base
#        for cs_salt in cs_salts:
#            base_datas=[]
#            for N in Ns:
#                n=int(n_particles/N)
#                base_file_Path = '../data/cpH-real_N-{}_n-{}_cM-0.0100_pKa-0.00_pKb-6.30_cS-{}.csv'.format(N, n, cs_salt)
#                base_data=pd.read_csv(base_file_Path)
#                base_datas.append(base_data)
#                #################                
#            plot_charge(
#                #acid_datas=acid_datas,
#                base_datas=base_datas,
#                fontsize=fontsize,
#                cs_salts=cs_salts,
#                MPCs=Ns,
#            )
#            plt.legend(loc='upper right',fontsize=fontsize,facecolor='White',framealpha=1.0,edgecolor='white',)
#            pdf.savefig()
#            plt.close()
#        for N in Ns:
#            base_datas=[]
#            for cs_salt in cs_salts:
#                n=int(n_particles/N)
#                base_file_Path = '../data/cpH-real_N-{}_n-{}_cM-0.0100_pKa-0.00_pKb-6.30_cS-{}.csv'.format(N, n, cs_salt)
#                base_data=pd.read_csv(base_file_Path)
#                base_datas.append(base_data)
#                #################                
#            plot_charge(
#                #acid_datas=acid_datas,
#                base_datas=base_datas,
#                fontsize=fontsize,
#                cs_salts=cs_salts,
#                #MPCs=Ns,
#            )
#            plt.legend(loc='upper right',fontsize=fontsize,facecolor='White',framealpha=1.0,edgecolor='white',)
#            pdf.savefig()
#            plt.close()

        # acid
        for cs_salt in cs_salts:
            acid_datas=[]
            for N in Ns:
                n=int(n_particles/N)
                acid_file_Path = '../data/cpH-real_N-{}_n-{}_cM-0.0100_pKa-4.00_pKb-0.00_cS-{}.csv'.format(N, n, cs_salt)
                acid_data=pd.read_csv(acid_file_Path)
                acid_datas.append(acid_data)
                #################
            plot_charge(
                acid_datas=acid_datas,
                #base_datas=base_datas,
                fontsize=fontsize,
                cs_salts=cs_salts,
                MPCs=Ns,
            )
            plt.legend(loc='upper right',fontsize=fontsize,facecolor='White',framealpha=1.0,edgecolor='white',)
            pdf.savefig()
            plt.close()
        for N in Ns:
            acid_datas=[]
            for cs_salt in cs_salts:
                n=int(n_particles/N)
                acid_file_Path = '../data/cpH-real_N-{}_n-{}_cM-0.0100_pKa-4.00_pKb-0.00_cS-{}.csv'.format(N, n, cs_salt)
                acid_data=pd.read_csv(acid_file_Path)
                acid_datas.append(acid_data)
                #################
            plot_charge(
                acid_datas=acid_datas,
                #base_datas=base_datas,
                fontsize=fontsize,
                cs_salts=cs_salts,
                #MPCs=Ns,
            )
            plt.legend(loc='upper right',fontsize=fontsize,facecolor='White',framealpha=1.0,edgecolor='white',)
            pdf.savefig()
            plt.close()

