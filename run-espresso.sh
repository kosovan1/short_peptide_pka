#!/bin/bash
# ====================================================
# for running jobs using Espresso
# ====================================================


#PBS -l select=1:ncpus=1:mem=500mb:scratch_local=2gb
#PBS -l walltime=2:00:00
#PBS -N peptide_simulation
trap 'clean_scratch' TERM EXIT
trap 'cp -r $SCRATCHDIR/temporary $DATADIR && clean_scratch ' TERM


# DATA
# ====================================================
job_name="META_peptide_simulation"

DATADIR="/storage/vestec1-elixir/home/nejedlama"
cp $DATADIR/$job_name.py $SCRATCHDIR || exit 1
mkdir -p $SCRATCHDIR/sugar_library || exit 1
cp $DATADIR/sugar_library/sugar.py $SCRATCHDIR/sugar_library/ || exit 1
cd $SCRATCHDIR || exit 2
WORKDIR=$job_name
mkdir $WORKDIR

wd="$SCRATCHDIR/$WORKDIR"
cp $job_name.py $wd || exit 2
cd $wd


# COMPUTATIONAL PART
# ======================================================
module add espresso_md-4.1.4

mpirun -n 1 pypresso $job_name.py

# COPY DATA AND LEAVE
# =======================================================
#ls $SCRATCHDIR
#mv $SCRATCHDIR/Seq* $WORKDIR
cd ..
cp -r $WORKDIR $DATADIR || export CLEAN_SCRATCH=false 

exit 0


