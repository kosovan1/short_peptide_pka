import numpy as np
import scipy.constants  # physical constants
import os
import csv 

import espressomd
import pint  # module for working with units and dimensions
from espressomd import electrostatics, polymer, reaction_ensemble
from espressomd.interactions import HarmonicBond

ureg = pint.UnitRegistry()
# sigma=0.355 nm is a commonly used  particle size in coarse-grained simulations
ureg.define('sigma = 0.355 * nm = sig')
sigma = 1.0 * ureg.sigma  # variable that has the value and dimension of one sigma
# N_A is the numerical value of Avogadro constant in units 1/mole
N_A = scipy.constants.N_A/ureg.mole
Bjerrum = 0.715 * ureg.nanometer  # Bjerrum length at 300K
# define that concentration is a quantity that must have a value and a unit
concentration = ureg.Quantity

# System parameters
#############################################################
# 0.01 mol/L is a reasonable concentration that could be used in experiments
c_mono = concentration(0.01, 'mol/L')
# Using the constant-pH method is safe if Ionic_strength > max(10**(-pH), 10**(-pOH) ) and C_salt > C_acid
# additional salt to control the ionic strength
#c_salt = concentration(2*c_mono)
c_salt = concentration(0.15, 'mol/L')
# In the ideal system, concentration is arbitrary (see Henderson-Hasselbalch equation)
# but it is important in the interacting system

PROB_REACTION = 0.5  # select the reaction move with 50% probability
# probability of the reaction is adjustable parameter of the method that affects the speed of convergence

# Simulate an interacting system with steric repulsion (Warning: it will be slower than without WCA!)
USE_WCA =   True
# Simulate an interacting system with electrostatics (Warning: it will be very slow!)
USE_ELECTROSTATICS =  True
#reaction_type='acid'
reaction_type='base'
MPC = 20
N_chains = 1 # number of chains

if reaction_type=='acid':
    N_acid = MPC*N_chains  # number of acid titratable units in the box
elif reaction_type=='base':
    N_base = MPC*N_chains   # number of base titratable  units in the box
print('c_mono', c_mono,)
print('c_salt', c_salt)
print('MPC', MPC, 'N_chains', N_chains)
# particle types of different species
if reaction_type=='acid':
    TYPES = {"HA": 0,"A":1, "Na": 2,"Cl": 3}
elif reaction_type=='base':
    TYPES = {"BH": 0,"B":1, "Na": 2,"Cl": 3}
else:
    print('define reaction_type')
CHARGES =  {"HA": 0,"A":-1, "BH":+1, "B":0, "Na": +1,"Cl": -1}
# acidity constant
pKa = 0.0
pKb = 0.0
if reaction_type=='acid':
    pKa = 4.0 # D amino acid
    Ka = 10**(-pKa)
elif reaction_type=='base':
    pKb = 6.3 #H amino acid
    Kb = 10**(-pKb)
pKw = 14.0  # autoprotolysis constant of water

# dependent parameters
if reaction_type=='acid':
    Box_V = (N_acid/N_A/c_mono)
elif reaction_type=='base':
    Box_V = (N_base/N_A/c_mono)
Box_L = np.cbrt(Box_V.to('m**3'))
if tuple(map(int, pint.__version__.split('.'))) < (0, 10):
    Box_L *= ureg('m')
# we shall often need the numerical value of box length in sigma
Box_L_in_sigma = Box_L.to('sigma').magnitude
# unfortunately, pint module cannot handle cube root of m**3, so we need to explicitly set the unit
N_salt = int(c_salt*Box_V*N_A)  # number of salt ion pairs in the box
# print the values of dependent parameters to check for possible rounding errors
print("N_salt: {0:.1f}, MPC: {1:.1f}, N_salt/MPC: {2:.7f}, c_salt/c_mono: {3:.7f}".format(
    N_salt, MPC, 1.0*N_salt/MPC, c_salt/c_mono))
print('Box_L_in_sigma', Box_L_in_sigma, 'Box_L',Box_L )

n_blocks = 16  # number of block to be used in data analysis
desired_block_size = 5*MPC*N_chains  # desired number of samples per block
# number of reaction samples per each pH value
num_samples = int(n_blocks * desired_block_size  / PROB_REACTION)
pH_value =  4.0

# Initialize the ESPResSo system
##############################################
system = espressomd.System(box_l=[Box_L_in_sigma] * 3)
system.time_step = 0.01
system.cell_system.skin = 0.4
system.thermostat.set_langevin(kT=1.0, gamma=1.0, seed=7)
np.random.seed(seed=10)  # initialize the random number generator in numpy
# create the particles

# create the particles
##################################################
# we need to define bonds before creating polymers
hb = HarmonicBond(k=10, r_0=2.0)
system.bonded_inter.add(hb)

# create the polymer composed of ionizable acid groups, initially in the ionized state
polymers = polymer.positions(n_polymers=N_chains,
                             beads_per_chain=MPC,
                             bond_length=0.9, seed=23)

for polymer in polymers:
    for index, position in enumerate(polymer):
        id = len(system.part)
        if reaction_type=='acid':
            system.part.add(id=id, pos=position, type=TYPES["A"], q=CHARGES["A"])
        elif  reaction_type=='base':
            system.part.add(id=id, pos=position, type=TYPES["BH"], q=CHARGES["BH"])
        if index > 0:
            system.part[id].add_bond((hb, id - 1))

if reaction_type=='acid':
    # add the corresponding number of H+ ions
    for index in range(MPC*N_chains):
        system.part.add(pos=np.random.random(3)*Box_L_in_sigma, type=TYPES["Na"], q=CHARGES["Na"])
elif reaction_type=='base':
     for index in range(MPC*N_chains):
        system.part.add(pos=np.random.random(3)*Box_L_in_sigma, type=TYPES["Cl"], q=CHARGES["Cl"])   
# add salt ion pairs
for index in range(N_salt):
    system.part.add(pos=np.random.random(
        3)*Box_L_in_sigma, type=TYPES["Na"], q=CHARGES["Na"])
    system.part.add(pos=np.random.random(
        3)*Box_L_in_sigma, type=TYPES["Cl"], q=CHARGES["Cl"])

# set up the WCA interaction between all particle pairs
if USE_WCA:
    #types = [TYPE_HA, TYPE_A, TYPE_B, TYPE_Na, TYPE_Cl]
    for type_1 in TYPES.values():
        for type_2 in TYPES.values():
            if type_1 >= type_2: #TODO
                system.non_bonded_inter[type_1, type_2].lennard_jones.set_params(
                    epsilon=1.0, sigma=1.0,
                    cutoff=2**(1.0 / 6), shift="auto")

# run a steepest descent minimization to relax overlaps
system.integrator.set_steepest_descent(
    f_max=0, gamma=0.1, max_displacement=0.1)
system.integrator.run(20)
system.integrator.set_vv()  # to switch back to velocity Verlet

# if needed, set up and tune the Coulomb interaction
if USE_ELECTROSTATICS:
    print("set up and tune p3m, please wait....")
    p3m = electrostatics.P3M(prefactor=Bjerrum.to(
        'sigma').magnitude, accuracy=1e-3)
    system.actors.add(p3m)
    p3m_params = p3m.get_params()
#    for key in list(p3m_params.keys()):
#        print("{} = {}".format(key, p3m_params[key]))
    print(p3m.get_params())
    print("p3m, tuning done")
else:
    # this speeds up the simulation of dilute systems with small particle numbers
    system.cell_system.set_n_square()

print("Done adding particles and interactions")

RE = reaction_ensemble.ConstantpHEnsemble(
    temperature=1, exclusion_radius=1.0, seed=77)
if reaction_type=='acid':
    RE.add_reaction(gamma=Ka, reactant_types=[TYPES["HA"]], reactant_coefficients=[1],
                product_types=[TYPES["A"], TYPES["Na"]], product_coefficients=[1, 1],
                default_charges={TYPES["HA"]: CHARGES["HA"], TYPES["A"]: CHARGES["A"], TYPES["Na"]: CHARGES["Na"]})
elif reaction_type=='base':
    RE.add_reaction(gamma=Kb, reactant_types=[TYPES["BH"]], reactant_coefficients=[1],
                product_types=[TYPES["B"], TYPES["Na"]], product_coefficients=[1, 1],
                default_charges={TYPES["BH"]: CHARGES["BH"], TYPES["B"]: CHARGES["B"], TYPES["Na"]: CHARGES["Na"]})    
print(RE.get_status())
if USE_WCA:
    method_name = 'cpH-real'
else:
    method_name = 'cpH-ideal'
sysname = "{0:s}".format(method_name) \
    + "_N-{:.0f}".format(MPC) \
    + "_n-{:.0f}".format(N_chains) \
    + "_cM-{:.4f}".format(c_mono.to('mol/l').magnitude) \
    + "_pKa-{:.2f}".format(pKa) \
    + "_pKb-{:.2f}".format(pKb) \
    + "_pH-{:.2f}".format(pH_value) \
    + "_cS-{:.3f}".format(c_salt.to('mol/l').magnitude) \

print("sysname:", sysname)
basename = os.path.basename(__file__)
print("basename:", basename)
template_name = "testing-acid-base-rexn.py"
new_name = sysname+".py"
if(basename == new_name):
    print("basename OK, run")
    pass
elif(basename == template_name):
    print("basename is template, create the script and run")
    #if( os.path.exists(new_name) ):
    #        raise ValueError( "\n\tScript " + new_name + " already exists, please check and delete it if desired.\n" )
    os.system( 'cp {} {}'.format(template_name, new_name) )
#    sys.exit()
else:
    raise ValueError("Unknown basename {}, should be either {} or {}".format(basename, sysname, template_name) )
#sys.exit()

write_csv = open(sysname + ".csv", "w")
observables =  ["time", "N_A", "N_BH", "alpha_acid", "alpha_base"]
# create the csv writer
output_file = csv.writer(write_csv)
# write a row to the csv file
output_file.writerow(observables)


def calc_observables(observables=[]):
    N_A = 0
    N_BH = 0
    alpha_acid = 0
    alpha_base = 0
    if reaction_type=='acid':
        N_A =  system.number_of_particles(type=TYPES["A"])
        alpha_acid = N_A / (N_acid)
    elif reaction_type=='base':
        N_BH =  system.number_of_particles(type=TYPES["BH"])
        alpha_base = N_BH / (N_base)
    result = {}
    results = []
    for obs in observables:
        if obs == "time":
            result[obs] = system.time
            results.append(system.time)
        elif obs == "N_A":
            result[obs] = N_A
            results.append(N_A)
        elif obs == "N_BH":
            result[obs] = N_BH
            results.append(N_BH)
        elif obs == "alpha_acid":
            result[obs] = alpha_acid
            results.append(alpha_acid)
        elif obs == "alpha_base":
            result[obs] = alpha_base
            results.append(alpha_base)
##        #else:
##        #    raise ValueEror("Unknown observable " + obs + "\n")
    output_file.writerow(results)
#    output_file.flush()
    return result


next_i = 0
# calculate the observables before first sampling
calc_observables(observables=observables)
RE.constant_pH = pH_value
RE.reaction(20*MPC*N_chains + 1) # pre-equilibrate to the new pH value
print("perform {:d} simulation runs".format(num_samples))
for i in range(num_samples):
    if np.random.random() < PROB_REACTION:
        # should be at least one reaction attempt per particle
        RE.reaction(MPC*N_chains + 1)
    elif USE_WCA:
        system.integrator.run(steps=1000)
    obs = calc_observables(observables=observables)
    if(i == next_i ):
        print(f"run {i:d} time {system.time:.3g} completed {i/num_samples*100:.2f}%  current values: alpha_acid {obs['alpha_acid']:.3f}  alpha_base {obs['alpha_base']:.3f} ")
        if(i==0):
            i=1
            next_i = 1
        else:
            next_i = next_i * 2
print("Finished")





