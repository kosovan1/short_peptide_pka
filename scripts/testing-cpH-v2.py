import matplotlib.pyplot as plt
import numpy as np
import scipy.constants  # physical constants

import espressomd
import pint  # module for working with units and dimensions
from espressomd import electrostatics, polymer, reaction_ensemble
from espressomd.interactions import HarmonicBond

ureg = pint.UnitRegistry()
# sigma=0.355 nm is a commonly used  particle size in coarse-grained simulations
ureg.define('sigma = 0.355 * nm = sig')
sigma = 1.0 * ureg.sigma  # variable that has the value and dimension of one sigma
# N_A is the numerical value of Avogadro constant in units 1/mole
N_A = scipy.constants.N_A/ureg.mole
Bjerrum = 0.715 * ureg.nanometer  # Bjerrum length at 300K
# define that concentration is a quantity that must have a value and a unit
concentration = ureg.Quantity

# System parameters
#############################################################
# 0.01 mol/L is a reasonable concentration that could be used in experiments
c_acid = concentration(0.01, 'mol/L')
# Using the constant-pH method is safe if Ionic_strength > max(10**(-pH), 10**(-pOH) ) and C_salt > C_acid
# additional salt to control the ionic strength
#c_salt = concentration(2*c_acid)
c_salt = concentration(0.01, 'mol/L')
# In the ideal system, concentration is arbitrary (see Henderson-Hasselbalch equation)
# but it is important in the interacting system
MPC = 1 
N_chain = 20
N_acid = N_chain * MPC  # number of titratable units in the box

PROB_REACTION = 0.5  # select the reaction move with 50% probability
# probability of the reaction is adjustable parameter of the method that affects the speed of convergence

# Simulate an interacting system with steric repulsion (Warning: it will be slower than without WCA!)
USE_WCA =  True
# Simulate an interacting system with electrostatics (Warning: it will be very slow!)
USE_ELECTROSTATICS = True

# particle types of different species
TYPE_HA = 0
TYPE_A = 1
TYPE_Na = 3
TYPE_Cl = 4

q_HA = 0
q_A = -1
q_Na = +1
q_Cl = -1

# acidity constant
pK = 4.88
K = 10**(-pK)
offset = 2.0  # range of pH values to be used pK +/- offset
num_pHs = 5  # number of pH values
pKw = 14.0  # autoprotolysis constant of water

# dependent parameters
Box_V = (N_acid/N_A/c_acid)
Box_L = np.cbrt(Box_V.to('m**3'))
if tuple(map(int, pint.__version__.split('.'))) < (0, 10):
    Box_L *= ureg('m')
# we shall often need the numerical value of box length in sigma
Box_L_in_sigma = Box_L.to('sigma').magnitude
# unfortunately, pint module cannot handle cube root of m**3, so we need to explicitly set the unit
N_salt = int(c_salt*Box_V*N_A)  # number of salt ion pairs in the box
# print the values of dependent parameters to check for possible rounding errors
print("N_salt: {0:.1f}, N_acid: {1:.1f}, N_salt/N_acid: {2:.7f}, c_salt/c_acid: {3:.7f}".format(
    N_salt, N_acid, 1.0*N_salt/N_acid, c_salt/c_acid))

n_blocks = 16  # number of block to be used in data analysis
desired_block_size = 10  # desired number of samples per block
# number of reaction samples per each pH value
num_samples = int(n_blocks * desired_block_size  / PROB_REACTION)
pHmin = pK-offset  # lowest pH value to be used
pHmax = pK+offset  # highest pH value to be used
pHs = np.linspace(pHmin, pHmax, num_pHs)  # list of pH values

# Initialize the ESPResSo system
##############################################
system = espressomd.System(box_l=[Box_L_in_sigma] * 3)
system.time_step = 0.01
system.cell_system.skin = 0.4
system.thermostat.set_langevin(kT=1.0, gamma=1.0, seed=7)
np.random.seed(seed=10)  # initialize the random number generator in numpy
# create the particles
##################################################
# we need to define bonds before creating polymers
hb = HarmonicBond(k=30, r_0=1.0)
system.bonded_inter.add(hb)

# create the polymer composed of ionizable acid groups, initially in the ionized state
polymers = polymer.positions(n_polymers=N_chain,
                             beads_per_chain=MPC,
                             bond_length=0.9, seed=23)
for polymer in polymers:
    for index, position in enumerate(polymer):
        id = len(system.part)
        system.part.add(id=id, pos=position, type=TYPE_A, q=q_A)
        if index > 0:
            system.part[id].add_bond((hb, id - 1))

#add the corresponding number of H+ ions
for index in range(N_acid):
    system.part.add(pos=np.random.random(3)*Box_L_in_sigma, type=TYPE_Na, q=q_Na)

# add salt ion pairs
for index in range(N_salt):
    system.part.add(pos=np.random.random(
        3)*Box_L_in_sigma, type=TYPE_Na, q=q_Na)
    system.part.add(pos=np.random.random(
        3)*Box_L_in_sigma, type=TYPE_Cl, q=q_Cl)

# set up the WCA interaction between all particle pairs
if USE_WCA:
    types = [TYPE_HA, TYPE_A, TYPE_Na, TYPE_Cl]
    for type_1 in types:
        for type_2 in types:
            if type_1 >= type_2: #TODO            
                system.non_bonded_inter[type_1, type_2].lennard_jones.set_params(
                    epsilon=1.0, sigma=1.0,
                    cutoff=2**(1.0 / 6), shift="auto")
# run a steepest descent minimization to relax overlaps
system.integrator.set_steepest_descent(
    f_max=0, gamma=0.1, max_displacement=0.1)
system.integrator.run(20)
system.integrator.set_vv()  # to switch back to velocity Verlet

# if needed, set up and tune the Coulomb interaction
if USE_ELECTROSTATICS:
    print("set up and tune p3m, please wait....")
    p3m = electrostatics.P3M(prefactor=Bjerrum.to(
        'sigma').magnitude, accuracy=1e-3)
    system.actors.add(p3m)
    p3m_params = p3m.get_params()
#    for key in list(p3m_params.keys()):
#        print("{} = {}".format(key, p3m_params[key]))
    print(p3m.get_params())
    print("p3m, tuning done")
else:
    # this speeds up the simulation of dilute systems with small particle numbers
    system.cell_system.set_n_square()

print("Done adding particles and interactions")

RE = reaction_ensemble.ConstantpHEnsemble(
    temperature=1, exclusion_radius=1.0, seed=77)

RE.add_reaction(gamma=K, reactant_types=[TYPE_HA], reactant_coefficients=[1],
                product_types=[TYPE_A, TYPE_Na], product_coefficients=[1, 1],
                default_charges={TYPE_HA: q_HA, TYPE_A: q_A, TYPE_Na: q_Na})
print(RE.get_status())

# the reference data from Henderson-Hasselbalch equation
def ideal_alpha(pH, pK):
    return 1. / (1 + 10**(pK - pH))


# empty lists as placeholders for collecting data
numAs_at_each_pH = []  # number of A- species observed at each sample

# run a productive simulation and collect the data
print("Simulated pH values: ", pHs)
for pH in pHs:
    print("Run pH {:.2f} ...".format(pH))
    RE.constant_pH = pH
    numAs_current = []  # temporary data storage for a given pH
    RE.reaction(20*N_acid + 1)  # pre-equilibrate to the new pH value
    for i in range(num_samples):
        if np.random.random() < PROB_REACTION:
            # should be at least one reaction attempt per particle
            RE.reaction(N_acid + 1)
        elif USE_WCA:
            system.integrator.run(steps=1000)
        numAs_current.append(system.number_of_particles(type=TYPE_A))
    numAs_at_each_pH.append(numAs_current)
    print("measured number of A-: {0:.2f}, (ideal: {1:.2f})".format(
        np.mean(numAs_current), N_acid*ideal_alpha(pH, pK)))
print("finished")

# statistical analysis of the results
def block_analyze(input_data, n_blocks=16):
    data = np.array(input_data)
    block = 0
    # this number of blocks is recommended by Janke as a reasonable compromise
    # between the conflicting requirements on block size and number of blocks
    block_size = int(data.shape[1] / n_blocks)
    print("block_size:", block_size)
    # initialize the array of per-block averages
    block_average = np.zeros((n_blocks, data.shape[0]))
    # calculate averages per each block
    for block in range(0, n_blocks):
        block_average[block] = np.average(
            data[:, block * block_size: (block + 1) * block_size], axis=1)
    # calculate the average and average of the square
    av_data = np.average(data, axis=1)
    av2_data = np.average(data * data, axis=1)
    # calculate the variance of the block averages
    block_var = np.var(block_average, axis=0)
    # calculate standard error of the mean
    err_data = np.sqrt(block_var / (n_blocks - 1))
    # estimate autocorrelation time using the formula given by Janke
    # this assumes that the errors have been correctly estimated
    tau_data = np.zeros(av_data.shape)
    for val in range(0, av_data.shape[0]):
        if av_data[val] == 0:
            # unphysical value marks a failure to compute tau
            tau_data[val] = -1.0
        else:
            tau_data[val] = 0.5 * block_size * n_blocks / (n_blocks - 1) * block_var[val] \
                / (av2_data[val] - av_data[val] * av_data[val])
    return av_data, err_data, tau_data, block_size

# estimate the statistical error and the autocorrelation time using the formula given by Janke
av_numAs, err_numAs, tau, block_size = block_analyze(numAs_at_each_pH)
print("av = ", av_numAs)
print("err = ", err_numAs)
print("tau = ", tau)

# calculate the average ionization degree
av_alpha = av_numAs/N_acid
print("av_alpha = ", av_alpha)
err_alpha = err_numAs/N_acid

# plot the simulation results compared with the ideal titration curve
plt.figure(figsize=(10, 6), dpi=80)
plt.errorbar(pHs - pK, av_alpha, err_alpha, marker='o', linestyle='none',
             label=r"simulation")
pHs2 = np.linspace(pHmin, pHmax, num=50)
plt.plot(pHs2 - pK, ideal_alpha(pHs2, pK), label=r"ideal")
plt.xlabel('pH-p$K$', fontsize=16)
plt.ylabel(r'$\alpha$', fontsize=16)
plt.legend(fontsize=16)
plt.savefig("plot.pdf", format="pdf", bbox_inches="tight")
plt.show()
# check if the blocks contain enough data for reliable error estimates
print("uncorrelated samples per block:\nblock_size/tau = ",
      block_size/tau)
threshold = 10.  # block size should be much greater than the correlation time
if np.any(block_size / tau < threshold):
    print("\nWarning: some blocks may contain less than ", threshold, "uncorrelated samples."
          "\nYour error estimated may be unreliable."
          "\nPlease, check them using a more sophisticated method or run a longer simulation.")
    print("? block_size/tau > threshold ? :", block_size/tau > threshold)
else:
    print("\nAll blocks seem to contain more than ", threshold, "uncorrelated samples.\
    Error estimates should be OK.")



