#!/bin/bash

pHs="1.0 1.5 2.0 2.5 3.0 3.5 4.0 4.5 5.0 5.5 6.0 6.5 7.0 7.5 8.0 8.5 9.0 9.5 10.0 10.5 11.0 11.5 12.0"
#pHs="1.1 1.2"
seq="DDDD"
positions="0,1,2,3"
nsamples="5000"

for pH in $pHs; do

    sysname=Seq-${seq}_pH-${pH}_simulation-run
    echo "run ${sysname}"

    sed /sequence/s/DHDED/${seq}/ 'META_peptide_simulation.py' \
    | sed /pH_value\ \=/s/4/${pH}/  \
    | sed /residue_positions/s/0,2,4,6/${positions}/  \
    | sed /Samples_per_pH/s/100/${nsamples}/  \
    > ${sysname}.py

    ./pypresso-4.1.4 ${sysname}.py > ${sysname}.out  
    echo "done ${sysname}"
done
