file="observables_analyzed.txt"
echo -e "#pH \talpha\terr" > obs_values.txt
for pH in 2 2.5 3 3.5 4  4.5 5 5.5 6 6.5 7 7.5 8 8.5 9 9.5 10 10.5 11 11.5 12
do

	folder_name="pH$pH"
	cd $folder_name
	#sed -i -e 's/.$//' observables.txt
	#sed -i -e '1d'
	python3 ~/skirit/crosslinking/supporting_scripts/do_statistical_analysis.py
	a=`head -n 1 $file | cut  -f 2`
	a_err=`head -n 1 $file | cut  -f 3`
	cd ..
	echo -e "$pH\t$a\t$a_err"  >> obs_values.txt
done

