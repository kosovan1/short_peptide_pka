set terminal pngcairo size 1600,1000  dashed enhanced font 'Verdana,36'
set output 'test_acidbase.png'

set key center left reverse Left font 'Verdana,24' samplen 1.3
set border linewidth 5

set xlabel "pH"
set ylabel "Degree of ionization {/Symbol a}"
xmin = 1.9
xmax = 6.1
ymin = 0.0
ymax = 1.0 
set xrange [xmin:xmax]
set yrange [0:1]

pKa=4

# Extended DH formula for log of activity coefficient 
A=0.5085 # mol^{1/2} dm^{3/2}
B=0.3281 # nm^{-1} mol^{-1/2} dm^{3/2}
a=0.9 # nm

I=0.05 # mol/L
pGamma(I) = A*sqrt(I)/(1+B*a*sqrt(I))

alpha_id_acid(pH)=1/(1+10**(pKa-pH))
alpha_dh_acid(pH, I)=1/(1+10**(pKa-pH-pGamma(I)))

alpha_id_base(pH)=1/(1+10**(1*(pH-pKa)))
alpha_dh_base(pH, I)=1/(1+10**(1*(pH-pKa-pGamma(I))))

# horizontal and vertical guides to the eye
set arrow 1 nohead from xmin,0.5 to xmax,0.5 lw 3 lc 'gray' dt '.'
set arrow 2 nohead from pKa,ymin to pKa,ymax lw 3 lc 'gray' dt '.'

id_ac_prot="ideal/acidproton/obs_values.txt"
id_ac_hyd="ideal/acidhydroxi/obs_values.txt"
id_bs_prot="ideal/baseproton/obs_values.txt"
id_bs_hyd="ideal/basehydroxi/obs_values.txt"

in_ac_prot="interacting/acidproton/obs_values.txt"
in_ac_hyd="interacting/acidhydroxi/obs_values.txt"
in_bs_prot="interacting/baseproton/obs_values.txt"
in_bs_hyd="interacting/basehydroxi/obs_values.txt"

set label 1 'c_{ac}=5mM' at 4.5, 0.55
set label 2 'c_s=50mM'   at 4.5, 0.45

myps = 4.0
mylw = 3.0

plot \
alpha_id_acid(x)   w l lw mylw lc 'red' notitle,\
alpha_id_base(x)   w l lw mylw lc 'blue' notitle,\
alpha_dh_acid(x,I) w l lw mylw lc 'red' dt "-" notitle,\
alpha_dh_base(x,I) w l lw mylw lc 'blue' dt "-" notitle,\
in_ac_prot u 1:2 w p ps myps pt 8  lw mylw lc "red" notitle,\
in_ac_hyd  u 1:2 w p ps myps pt 10 lw mylw lc "red" notitle,\
in_bs_prot u 1:2 w p ps myps pt 8  lw mylw lc "blue" notitle,\
in_bs_hyd  u 1:2 w p ps myps pt 10 lw mylw lc "blue" notitle,\
in_ac_prot u ($1+pGamma(I)):2 w p ps myps pt 9  lw mylw lc "red" notitle,\
in_ac_hyd  u ($1-pGamma(I)):2 w p ps myps pt 11 lw mylw lc "red" notitle,\
in_bs_prot u ($1+pGamma(I)):2 w p ps myps pt 9  lw mylw lc "blue" notitle,\
in_bs_hyd  u ($1-pGamma(I)):2 w p ps myps pt 11 lw mylw lc "blue" notitle,\
NaN w l lc "black" lw mylw t 'HH',\
NaN w l lc "black" lw mylw dt "-" t 'HH+DH',\
NaN w l lw 5*mylw lc "red"  t 'Acid',\
NaN w l lw 5*mylw lc "blue"  t 'Base',\
NaN w p ps myps pt 14 lw mylw lc "black" t 'Uncorrected data',\
NaN w p ps myps pt 15 lw mylw lc "black" t 'Corrected data',\
NaN w p ps myps pt 9  lw mylw lc "black" t 'Counter-ion H^{+}',\
NaN w p ps myps pt 11 lw mylw lc "black" t 'Counter-ion OH^{-}'

#in_ac_prot u 1:2:3 w yerrorbars ps myps pt 8  lw mylw lc "red" notitle,\
#in_ac_hyd  u 1:2:3 w yerrorbars ps myps pt 10 lw mylw lc "red" notitle,\
#in_bs_prot u 1:2:3 w yerrorbars ps myps pt 8  lw mylw lc "blue" notitle,\
#in_bs_hyd  u 1:2:3 w yerrorbars ps myps pt 10 lw mylw lc "blue" notitle,\
#in_ac_prot u ($1+pGamma(I)):2:3 w yerrorbars ps myps pt 9  lw mylw lc "red" notitle,\
#in_ac_hyd  u ($1-pGamma(I)):2:3 w yerrorbars ps myps pt 11 lw mylw lc "red" notitle,\
#in_bs_prot u ($1+pGamma(I)):2:3 w yerrorbars ps myps pt 9  lw mylw lc "blue" notitle,\
#in_bs_hyd  u ($1-pGamma(I)):2:3 w yerrorbars ps myps pt 11 lw mylw lc "blue" notitle,\

#id_bs_prot u 1:2:3 w yerrorbars ps 0.5*myps pt 9 lc "blue" notitle,\
#id_ac_prot u 1:2:3 w yerrorbars ps 0.5*myps pt 9 lc "red" notitle,\
#id_bs_hyd  u 1:2:3 w yerrorbars ps 0.5*myps pt 11 lc "blue" notitle,\
#id_ac_hyd  u 1:2:3 w yerrorbars ps 0.5*myps pt 11 lc "red" notitle,\

set output 'residuals_acidbase.png'
ymin = -0.075
ymax = -ymin
set yrange [ymin:ymax]
set ylabel "Residuals: Simulation - (HH+DH)"
set arrow 1 nohead from xmin,0.0 to xmax,0.0 lw 3 lc 'gray60' lt 1
set arrow 2 nohead from pKa,ymin to pKa,ymax lw 3 lc 'gray40' lt '.'
p \
in_ac_prot u 1:( $2 - alpha_dh_acid($1,I) ):3 w yerr ps myps pt 8  lw mylw lc "red" notitle,\
in_ac_hyd  u 1:( $2 - alpha_dh_acid($1,I) ):3 w yerr ps myps pt 10 lw mylw lc "red" notitle,\
in_bs_prot u 1:( $2 - alpha_dh_base($1,I) ):3 w yerr ps myps pt 8  lw mylw lc "blue" notitle,\
in_bs_hyd  u 1:( $2 - alpha_dh_base($1,I) ):3 w yerr ps myps pt 10 lw mylw lc "blue" notitle,\
in_ac_prot u ($1+pGamma(I)):( $2 - alpha_dh_acid($1+pGamma(I),I) ):3 w yerr ps myps pt 9  lw mylw lc "red" notitle,\
in_ac_hyd  u ($1-pGamma(I)):( $2 - alpha_dh_acid($1-pGamma(I),I) ):3 w yerr ps myps pt 11 lw mylw lc "red" notitle,\
in_bs_prot u ($1+pGamma(I)):( $2 - alpha_dh_base($1+pGamma(I),I) ):3 w yerr ps myps pt 9  lw mylw lc "blue" notitle,\
in_bs_hyd  u ($1-pGamma(I)):( $2 - alpha_dh_base($1-pGamma(I),I) ):3 w yerr ps myps pt 11 lw mylw lc "blue" notitle,\

