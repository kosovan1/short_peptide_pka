import numpy as np
import pint
import time
import os
import espressomd
from espressomd import interactions
from espressomd import reaction_ensemble
from espressomd import electrostatics
from itertools import combinations_with_replacement


metacentrum= True       # Controls if the program is being run on metacentrum
units = pint.UnitRegistry()
time_ini=time.time()*units.s
walltime = 24*units("hour") # Time requested in metacentrum
N_A=6.02214076e23    / units.mol
TEMPERATURE = 298.15 * units.K
PARTICLE_SIZE = 0.355 * units.nm
Kb=1.38064852e-23    * units.J / units.K
e=1.60217662e-19 *units.C
units.define(f'reduced_energy = {TEMPERATURE * Kb}')
units.define(f'reduced_length = {PARTICLE_SIZE}')
KbT=1*units("reduced_energy")

# Simulation parameters

N_samples = 1e3              # Number of samples
N_frames=N_samples     # Number of trajectory snapshots
SEED = None                  # System seed, if None the program takes the system hour
time_step=1e-2
N_steps_MD = 1000
LJepsilon=1*units('reduced_energy')
LJsigma=1*units('reduced_length')
solvent_permittivity=70.5

# System general parameters

ideal=True
c_salt=5e-2*units('mol/L')
c_ionizable_group=5e-3*units('mol/L')
N_ionizable_group=25

# Acid-base reactions parameters

pKa=4 
pH= 7
acidity='acidic'
counter_ion='proton'
valid_keys=["acidic", "basic", "proton", "hydroxi"]
if acidity not in valid_keys:
    raise ValueError(acidity, valid_keys)    
if counter_ion not in valid_keys:
    raise ValueError(acidity, valid_keys)

# Sampling of the reactions

prob_MD = 0.5    # 1 - prob_MD = probability of performing one reaction

if metacentrum:
    N_samples = 5e4              # Number of samples
    acidity = "kkkkkk"
    counter_ion = "rrrrr"
    pH= ppppp
    ideal = mmmmm
    N_frames=1000  

if SEED is None:

    if metacentrum:

        job_id= ggggg
        SEED=job_id
        print("Job_id: ", job_id)

    else:

        SEED=int(time.time())

print("\n Random seed: ", SEED, "\n")

# Set-up the system

Box_V = N_ionizable_group/(N_A*c_ionizable_group)
Box_L = Box_V**(1./3.) # Length of the simulation box [nm]

print("Box length: ", Box_L.to('nm'), " or ", Box_L.to('reduced_length'))
np.random.seed(seed=SEED)

system = espressomd.System(box_l=[Box_L.to('reduced_length').magnitude] * 3)
N_salt = int((Box_V.to('reduced_length**3')*c_salt.to('mol/reduced_length**3')*N_A).magnitude)
c_salt_calculated=N_salt/(Box_V*N_A)
print('\n Added salt concentration of ', c_salt_calculated.to('mol/L'), 'given by ', N_salt)

type_map={"ionizable_group_protonated":0,
            "ionizable_group_unprotonated":1,
            "cation": 2,
            "anion":3}

print("type map:", type_map)

# Create particles in the system

if acidity == "acidic":
    ionization_type=type_map["ionizable_group_protonated"]
elif acidity == "basic":
    ionization_type=type_map["ionizable_group_unprotonated"]

for _ in range(N_ionizable_group):
    system.part.add(pos=np.random.random(3) * system.box_l, type=ionization_type)

for _ in range(N_salt):
    system.part.add(pos=np.random.random(3) * system.box_l, type=type_map["cation"], q=1)
    system.part.add(pos=np.random.random(3) * system.box_l, type=type_map["anion"], q=-1)
    

stats_file=open("observables.txt",mode='w')

if not ideal:

    # Lennard-Jones interactions
    for type_pair in combinations_with_replacement(type_map.values(), 2):

        system.non_bonded_inter[type_pair[0], type_pair[1]].lennard_jones.set_params(epsilon = LJepsilon.to('reduced_energy').magnitude,
                                                                                sigma = LJsigma.to('reduced_length').magnitude,
                                                                                cutoff = 2**(1./6.)*LJsigma.to('reduced_length').magnitude,
                                                                                offset = 0,
                                                                                shift = "auto")
    print("\n WCA successfully added to the system \n")
    
    BJERRUM_LENGTH = e**2 / (4 * units.pi * units.eps0 * solvent_permittivity * KbT)

    print('\n Bjerrum length ', BJERRUM_LENGTH.to('nm'), '=', BJERRUM_LENGTH.to('reduced_length'))

    COULOMB_PREFACTOR=BJERRUM_LENGTH.to('reduced_length') * KbT.to('reduced_energy')

    coulomb = espressomd.electrostatics.P3M(prefactor = COULOMB_PREFACTOR.magnitude, accuracy=1e-2)

        
    system.time_step=0.01
    system.actors.add(coulomb)

    # save the optimal parameters and add them by hand

    p3m_params = coulomb.get_params()
    system.actors.remove(coulomb)
    coulomb = electrostatics.P3M(
                                prefactor = COULOMB_PREFACTOR.magnitude,
                                accuracy = 1e-3,
                                mesh = p3m_params['mesh'],
                                alpha = p3m_params['alpha'] ,
                                cao = p3m_params['cao'],
                                r_cut = p3m_params['r_cut'],
                                tune = False
                                )

    system.actors.add(coulomb)
    print("\n Electrostatics successfully added to the system \n")



    print("\n*** Minimazing system energy... ***\n")

    system.cell_system.skin = 1
    system.time_step=time_step
    print("steepest descent")
    system.integrator.set_steepest_descent(f_max=0, gamma=0.1, max_displacement=0.1)
    system.integrator.run(1000)
    print("velocity verlet")
    system.integrator.set_vv()  # to switch back to velocity Verlet
    system.integrator.run(1000)
    system.thermostat.turn_off()
    # Reset the time of the system to 0
    system.time = 0.
    
    print("\n Minimization finished \n")


# Setup acid-base reaction
RE = espressomd.reaction_ensemble.ConstantpHEnsemble(
        temperature=KbT.magnitude, exclusion_radius=LJsigma.to("reduced_length").magnitude, seed=SEED)
if acidity == "acidic":
    charge_map={type_map["ionizable_group_protonated"]:0,
            type_map["ionizable_group_unprotonated"]:-1,
            type_map["cation"]:+1,
            type_map["anion"]:-1}
elif acidity == "basic":
    charge_map={type_map["ionizable_group_protonated"]:+1,
            type_map["ionizable_group_unprotonated"]:0,
            type_map["cation"]:+1,
            type_map["anion"]:-1}

if counter_ion == "proton":
    RE.constant_pH = pH
    K_A=10**-(pKa)
    RE.add_reaction(gamma=K_A, reactant_types=[type_map["ionizable_group_protonated"]], reactant_coefficients=[1],
                    product_types=[type_map["ionizable_group_unprotonated"], type_map["cation"]], product_coefficients=[1, 1],
                    default_charges=charge_map)
elif counter_ion == "hydroxi":
    K_B=10**-(14-pKa)
    RE.constant_pH = 14-pH
    RE.add_reaction(gamma=K_B, reactant_types=[type_map["ionizable_group_unprotonated"]], reactant_coefficients=[1],
                    product_types=[type_map["ionizable_group_protonated"], type_map["anion"]], product_coefficients=[1, 1],
                    default_charges=charge_map)


system.time_step=1e-2
system.integrator.set_vv()
system.thermostat.set_langevin(kT=KbT.magnitude, gamma=0.1, seed=SEED)

# Optimize the value of skin

print("\n*** Optimizing skin ... ***")

system.cell_system.tune_skin(min_skin=1, max_skin=system.box_l[0]/2, tol=1e-3, int_steps=1000, adjust_max_skin=True)

print("Optimized skin value: ", system.cell_system.skin, "\n")

stats_file.write("#time\talpha\tN_prot\tN_unprot\n")

# The trajectories of the simulations will be stored using espresso built-up functions in separed files in the folder 'frames'
from espressomd.io.writer import vtf
if not os.path.exists('./frames'):
    os.makedirs('./frames')


print()
print("*** Starting simulation ***")
print()

N_time=2
N_frame=0
N_steps_print=int(N_samples/N_frames)
system.setup_type_map(type_map.values())
system.time=0

for step in range(int(N_samples)):
    rand=np.random.random()

    if rand <= prob_MD: # Molecular Dynamics 

        system.integrator.run(steps=N_steps_MD)

    else:

        RE.reaction(N_ionizable_group)

    # Store observables

    N_prot=system.number_of_particles(type=type_map['ionizable_group_protonated'])
    N_unprot=system.number_of_particles(type=type_map['ionizable_group_unprotonated'])
    
    if acidity == "acidic":
        alpha=N_unprot/N_ionizable_group
    else:
        alpha=N_prot/N_ionizable_group

    obs_list=[alpha, N_prot,N_unprot]
    stats_file.write(str(system.time))
    for obs in obs_list:
        stats_file.write('\t'+str(obs))
    stats_file.write('\n')

    if (step % N_steps_print == 0):

        N_frame+=1
        with open('frames/trajectory'+str(N_frame)+'.vtf', mode='w+t') as coordinates:
                vtf.writevsf(system, coordinates)
                vtf.writevcf(system, coordinates)

    if (step == int(N_time)):

        N_time=N_time*1.5
        time_act=time.time()*units.s
        perc_sim=100 *(step+1) / (N_samples)
        time_per_step= (time_act - time_ini)/(step+1)
        remaining_time=(N_samples - step +1)*time_per_step
        elapsed_time=time_act-time_ini

        def find_right_time_units(time):
            """
            Given a pint variable with time units, it returns in which time scale it is
            """

            if (time.to('s').magnitude/60 < 1):

                time_unit='s'

            elif (time.to('s').magnitude/3600 < 1):

                time_unit='min'

            elif (time.to('s').magnitude/(3600*24) < 1):

                time_unit='hour'

            else:

                time_unit='day'

            return time_unit

        time_unit_elapsed_time=find_right_time_units(elapsed_time)
        time_unit_remaining_time=find_right_time_units(remaining_time)

        print("{0:.2g}% done, elapsed time {1:.2g}s; estimated completion in {2:.2g}s".format(perc_sim,elapsed_time.to(time_unit_elapsed_time),remaining_time.to(time_unit_remaining_time)))

    if (time.time() - time_ini.to("s").magnitude > walltime.to("s").magnitude*0.9):
        print("Main loop exited promptly at step ", step)
        break


    
